/*
 *  Copyright 2008-2009 NVIDIA Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */


#include <iostream>
#include <stdio.h>
#include "cmdline.h"
#include "gallery.h"
#include "sparse_io.h"
#include "sparse_conversions.h"
#include "rdtsc.h"
#include "config.h"

#include "omp.h"
#include "mkl.h"
#include "mkl_spblas.h"

#define FREQ_CPU 3.33e9  
#define NUM_LOOPS 500

//Pack MKL functions
//DIA
template <typename IndexType, typename ValueType>
void mkl_diagemv(char *transa, IndexType *m, ValueType *val, IndexType *lval,  IndexType *idiag, IndexType *ndiag, ValueType *x,  ValueType *y)
{
    tmp_mkl_diagemv(transa, m, val, lval, idiag, ndiag, x, y);
}

void tmp_mkl_diagemv(char *transa, int *m, float *val, int *lval,  int *idiag, int *ndiag, float *x,  float *y)
{
    mkl_sdiagemv(transa, m, val, lval, idiag, ndiag, x, y);
}

void tmp_mkl_diagemv(char *transa, int *m, double *val, int *lval,  int *idiag, int *ndiag, double *x,  double *y)
{
    mkl_ddiagemv(transa, m, val, lval, idiag, ndiag, x, y);
}

//CSR
template <typename IndexType, typename ValueType>
void mkl_csrgemv(char *transa, IndexType *m, ValueType *a, IndexType *ia,  IndexType *ja, ValueType *x,  ValueType *y)
{
    tmp_mkl_csrgemv(transa, m, a, ia, ja, x, y);
}

void tmp_mkl_csrgemv(char *transa, int *m, float *a, int *ia,  int *ja, float *x,  float *y)
{
    mkl_scsrgemv(transa, m, a, ia, ja, x, y);
}

void tmp_mkl_csrgemv(char *transa, int *m, double *a, int *ia,  int *ja, double *x,  double *y)
{
    mkl_dcsrgemv(transa, m, a, ia, ja, x, y);
}

//COO
template <typename IndexType, typename ValueType>
void mkl_coogemv(char *transa, IndexType *m, ValueType *val, IndexType *rowind,  IndexType *colind, IndexType *nnz, ValueType *x,  ValueType *y)
{
    tmp_mkl_coogemv(transa, m, val, rowind, colind, nnz, x, y);
}

void tmp_mkl_coogemv(char *transa, int *m, float *val, int *rowind,  int *colind, int *nnz, float *x,  float *y)
{
    mkl_scoogemv(transa, m, val, rowind, colind, nnz, x, y);
}

void tmp_mkl_coogemv(char *transa, int *m, double *val, int *rowind,  int *colind, int *nnz, double *x,  double *y)
{
    mkl_dcoogemv(transa, m, val, rowind, colind, nnz, x, y);
}



void usage(int argc, char** argv)
{
    std::cout << "Usage:\n";
    std::cout << "\t" << argv[0] << " with following parameters:\n";
    std::cout << "\t" << " my_matrix.mtx\n";
    std::cout << "\t" << " --precision=32(or 64)\n";
    std::cout << "\t" << " --max_diags_limit=20\n";
    std::cout << "\t" << " --dia_fill_ratio=0.95\n";
    std::cout << "\t" << " --min_diags_ratio=0.1\n";
    std::cout << "\t" << " --ell_max_cols=100\n";
    std::cout << "\t" << " --ell_min_deviation=1\n";
    std::cout << "Note: my_matrix.mtx must be real-valued sparse matrix in the MatrixMarket file format.\n"; 
}

template <typename IndexType, typename ValueType>
void print_y(ValueType *y, int n,char *file)
{
  FILE *fp=fopen(file,"w");
  int i;
  for(i=0;i<n;i++)
  {
    fprintf(fp,"%.2f\n",y[i]);
  }
  fclose(fp);
}

template <typename IndexType, typename ValueType>
void run_all_kernels(int argc, char **argv)
{
    int num_loops = NUM_LOOPS;
    char * mm_filename = NULL;
    for(int i = 1; i < argc; i++){
        if(argv[i][0] != '-'){
            mm_filename = argv[i];
            break;
        }
    }

    csr_matrix<IndexType,ValueType> csr;
    
    if(mm_filename == NULL)
    {
        printf("No input MM file!\n");
        return;
    }
    else
        csr = read_csr_matrix<IndexType,ValueType>(mm_filename);
    printf("file=%s \nrows=%d cols=%d nonzeros=%d\n", mm_filename, csr.num_rows, csr.num_cols, csr.num_nonzeros);

    FILE *fp_perf = fopen(PERF_FILE, "w");

#ifdef TESTING
//print in CSR format
    printf("Writing matrix in CSR format to test_CSR ...\n");
    FILE *fp = fopen("test_CSR", "w");
    fprintf(fp, "%d\t%d\t%d\n", csr.num_rows, csr.num_cols, csr.num_nonzeros);
    fprintf(fp, "csr.Ap:\n");
    for (IndexType i=0; i<csr.num_rows+1; i++)
    {
      fprintf(fp, "%d  ", csr.Ap[i]);
    }
    fprintf(fp, "\n\n");
    fprintf(fp, "csr.Aj:\n");
    for (IndexType i=0; i<csr.num_nonzeros; i++)
    {
      fprintf(fp, "%d  ", csr.Aj[i]);
    }
    fprintf(fp, "\n\n");
    fprintf(fp, "csr.Ax:\n");
    for (IndexType i=0; i<csr.num_nonzeros; i++)
    {
      fprintf(fp, "%f  ", csr.Ax[i]);
    }
    fprintf(fp, "\n");
    fclose(fp);
#endif 

    // fill matrix with random values: some matrices have extreme values, 
    // which makes correctness testing difficult, especially in single precision
    srand(13);
    for(IndexType i = 0; i < csr.num_nonzeros; i++){
        csr.Ax[i] = 1.0 - 2.0 * (rand() / (RAND_MAX + 1.0)); 
    }
    
    IndexType num_rows = csr.num_rows;
    IndexType num_cols = csr.num_cols;
    IndexType num_nonzeros = csr.num_nonzeros;

    ValueType *x = (ValueType*)malloc(num_cols*sizeof(ValueType));
    ValueType *y1 = (ValueType*)malloc(num_rows*sizeof(ValueType));
    ValueType *y2 = (ValueType*)malloc(num_rows*sizeof(ValueType));
    for(IndexType i = 0; i < num_cols; i++)
        x[i] = rand() / (RAND_MAX + 1.0); 
    std::fill(y1, y1 + num_rows, 0);
    std::fill(y2, y2 + num_rows, 0);
    
    //SpMV process
    long long int start, end;
    double estimated_time, bytes_per_spmv, GFLOPs, GBYTEs;
  
#if 0
    //Initial CSR
    for (IndexType i = 0; i < num_rows; i++)
    {
        IndexType row_start = csr.Ap[i];
        IndexType row_end   = csr.Ap[i+1];
        ValueType sum = y1[i];

        for (IndexType jj = row_start; jj < row_end; jj++) 
        {
            IndexType j = csr.Aj[jj];  //column index
            sum += x[j] * csr.Ax[jj];
        }
        y1[i] = sum; 
    }
    print_y<IndexType, ValueType>(y1, num_rows, "csr_y");

    start = rdtsc();
    for ( int loop=0; loop<num_loops; loop++)
    {
      for (IndexType i = 0; i < num_rows; i++)
      {
          IndexType row_start = csr.Ap[i];
          IndexType row_end   = csr.Ap[i+1];
          ValueType sum = y1[i];

          for (IndexType jj = row_start; jj < row_end; jj++) 
          {
              IndexType j = csr.Aj[jj];  //column index
              sum += x[j] * csr.Ax[jj];
          }
          y1[i] = sum; 
      }
    }
    end = rdtsc();
    estimated_time = (end-start)/FREQ_CPU/num_loops;
    bytes_per_spmv = sizeof(IndexType) * (2*num_rows+num_nonzeros) + sizeof(ValueType) * (2*num_nonzeros+2*num_rows);
    GFLOPs = (estimated_time == 0) ? 0 : (2.0 * (double) num_nonzeros / estimated_time) / 1e9;
    GBYTEs = (estimated_time == 0) ? 0 : (bytes_per_spmv / estimated_time) / 1e9;
    printf("Benchmarking CSR: %8.4f ms ( %5.2f GFLOP/s %5.1f GB/s)\n\n", estimated_time*1000, GFLOPs, GBYTEs); 
#endif      
      
    //MKL test
    
#if 1
    //COO
    // CREATE COO MATRIX 
    printf("\tcreating coo_matrix:");
    coo_matrix<IndexType,ValueType> coo = csr_to_coo<IndexType,ValueType>(csr);
    printf("\n");

    mkl_coogemv("N", &num_rows, coo.V, coo.I,  coo.J, &num_nonzeros, x, y2);
//    print_y<IndexType, ValueType>(y2, num_rows, "mkl_y");
    std::fill(y2, y2 + num_rows, 0);

    start = rdtsc();
    for (int loop=0; loop<num_loops; loop++)
    {
        mkl_coogemv("N", &num_rows, coo.V, coo.I,  coo.J, &num_nonzeros, x, y2);
    }
    end = rdtsc();
    estimated_time = (end-start)/FREQ_CPU/num_loops;
    bytes_per_spmv = sizeof(IndexType) * (2*num_rows+num_nonzeros) + sizeof(ValueType) * (2*num_nonzeros+2*num_rows);
    GFLOPs = (estimated_time == 0) ? 0 : (2.0 * (double) num_nonzeros / estimated_time) / 1e9;
    GBYTEs = (estimated_time == 0) ? 0 : (bytes_per_spmv / estimated_time) / 1e9;
    printf("Benchmarking COO MKL: %8.4f ms ( %5.2f GFLOP/s %5.1f GB/s)\n\n", estimated_time*1000, GFLOPs, GBYTEs); 
    fprintf (fp_perf, "MKL COO perf : %.2f GFLOPS\n", GFLOPs);
    delete_host_matrix(coo);
#endif
    std::fill(y2, y2 + num_rows, 0);

#if 1
    //DIA
    IndexType max_diags =  static_cast<IndexType>( (20 * num_nonzeros) / num_rows + 1 );
    // CREATE DIA MATRIX
    printf("\tcreating dia_matrix:");
    dia_matrix<IndexType,ValueType> dia = csr_to_dia<IndexType,ValueType>(csr, max_diags);
    printf("\n");

    if (dia.num_nonzeros == 0 && csr.num_nonzeros != 0)
    {     
	    printf("\tNumber of diagonals (%d) excedes limit (%d)\n", dia.num_diags, max_diags);
      GFLOPs = 0;
      fprintf (fp_perf, "MKL DIA perf : %.2f GFLOPS\n", GFLOPs);
    }
    else
    {
    mkl_diagemv("N", &num_rows, dia.diag_data, &dia.stride, dia.diag_offsets, &dia.num_diags, x, y2);
//    print_y<IndexType, ValueType>(y2, num_rows, "mkl_y");
    std::fill(y2, y2 + num_rows, 0);

    start = rdtsc();
    for (int loop=0; loop<num_loops; loop++)
    {
        mkl_diagemv("N", &num_rows, dia.diag_data, &dia.stride, dia.diag_offsets, &dia.num_diags, x, y2);
    }
    end = rdtsc();
    estimated_time = (end-start)/FREQ_CPU/num_loops;
    bytes_per_spmv = sizeof(IndexType) * (2*num_rows+num_nonzeros) + sizeof(ValueType) * (2*num_nonzeros+2*num_rows);
    GFLOPs = (estimated_time == 0) ? 0 : (2.0 * (double) num_nonzeros / estimated_time) / 1e9;
    GBYTEs = (estimated_time == 0) ? 0 : (bytes_per_spmv / estimated_time) / 1e9;
    printf("Benchmarking DIA MKL: %8.4f ms ( %5.2f GFLOP/s %5.1f GB/s)\n\n", estimated_time*1000, GFLOPs, GBYTEs); 
    fprintf (fp_perf, "MKL DIA perf : %.2f GFLOPS\n", GFLOPs);
    }

    delete_host_matrix(dia);
    std::fill(y2, y2 + num_rows, 0);
#endif

#if 1
    //CSR
    mkl_csrgemv("N", &num_rows, csr.Ax, csr.Ap,  csr.Aj, x, y2);
//    print_y<IndexType, ValueType>(y2, num_rows, "mkl_y");
    std::fill(y2, y2 + num_rows, 0);

    start = rdtsc();
    for (int loop=0; loop<num_loops; loop++)
    {
        mkl_csrgemv("N", &num_rows, csr.Ax, csr.Ap,  csr.Aj, x, y2);
    }
    end = rdtsc();
    estimated_time = (end-start)/FREQ_CPU/num_loops;
    bytes_per_spmv = sizeof(IndexType) * (2*num_rows+num_nonzeros) + sizeof(ValueType) * (2*num_nonzeros+2*num_rows);
    GFLOPs = (estimated_time == 0) ? 0 : (2.0 * (double) num_nonzeros / estimated_time) / 1e9;
    GBYTEs = (estimated_time == 0) ? 0 : (bytes_per_spmv / estimated_time) / 1e9;
    printf("Benchmarking CSR MKL: %8.4f ms ( %5.2f GFLOP/s %5.1f GB/s)\n\n", estimated_time*1000, GFLOPs, GBYTEs); 
    fprintf (fp_perf, "MKL CSR perf : %.2f GFLOPS\n", GFLOPs);
    std::fill(y2, y2 + num_rows, 0);
#endif

    fflush(stdout);
    fflush(fp_perf);

    free(x);
    free(y1);
    free(y2);
    delete_host_matrix(csr);
    fclose(fp_perf);
}

int main(int argc, char** argv)
{
    if (get_arg(argc, argv, "help") != NULL){
        usage(argc, argv);
        return EXIT_SUCCESS;
    }

    int precision = 32;
    char * precision_str = get_argval(argc, argv, "precision");
    if(precision_str != NULL)
        precision = atoi(precision_str);
    printf("Using %d-bit floating point precision\n", precision);

    int tid, nthreads = 0;
#pragma omp parallel
    {
      tid = omp_get_thread_num();
      if ( tid == 0 )
        nthreads = omp_get_num_threads();
      printf("Thread : %d\n", tid);
    }
    printf ("Num_threads : %d\n", nthreads);

    if(precision ==  32)
    {
        run_all_kernels<int, float>(argc,argv);
    }
    else if(precision == 64)
    {
        run_all_kernels<int, double>(argc,argv);
    }
    else{
        usage(argc, argv);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

