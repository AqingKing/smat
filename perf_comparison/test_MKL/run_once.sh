#make clean; make
#icc -Wall -O3 -msse4.2 -fp-model source -o main main.cpp mmio.cpp -mkl=sequential
#./main /home/exchange/matrices/MM/TKK/t2d_q4/t2d_q4.mtx

icc -Wall -O3 -msse4.2 -o main main.cpp mmio.cpp -mkl=parallel -openmp
#./main /home/exchange/matrices/MM/TKK/t2d_q4/t2d_q4.mtx
./main /home/exchange/matrices/MM/Zitney/rdist3a/rdist3a.mtx
