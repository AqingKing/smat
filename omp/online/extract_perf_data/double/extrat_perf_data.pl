#!/usr/bin/perl

$nthreads = 12; # Need to change 

$matrices = "../../../../Figures/exp/best_mats/test_best_mats";
$out = "../../Estimate_results_double_$nthreads";
$out_perf = ">$nthreads.txt";

$line_num = 0;

open (OUT_PERF, $out_perf) || die "Cannot open $out_perf : $!\n";

print "Using $matrices input MM matrices\n";
open (MM_MAT, $matrices) || die "Cannot open $matrices : $!\n";
while ($mm_file = <MM_MAT>)
{
    $line_num ++;
    chomp $mm_file;
#    print $mm_file, "\n";
    @name = split ( /\//, $mm_file);  
    @tmp = split (/.mtx/, $name[-1]);
    mkdir "$out\/$name[5]";
    mkdir "$out\/$name[5]\/$name[6]";
    mkdir "$out\/$name[5]\/$name[6]\/$tmp[0]_perf";

    $perf_file = $out . "\/$name[5]\/$name[6]\/$tmp[0]_perf\/estimate_info";
#    printf ("%s\n\n", $perf_file);
    open (PERF_FILE, $perf_file) || die "Cannot open $perf_file : $!\n";
    while ($perf_line = <PERF_FILE>)
    {
        chomp $perf_line;
        @words = split (/ /, $perf_line);  
        if ( $words[0] eq "Estimate" )
        {
          printf OUT_PERF ("%f,", $words[-2]);
          last;
        }
    }
    close(PERF_FILE);
}
close(MM_MAT);
close(OUT_PERF);
printf ("Output to %s.\n", $out_perf);
