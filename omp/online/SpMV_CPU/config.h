//General defines
#define MAX_ITER 800

#define FREQ_CPU 2.67e9  
#define MEM_SIZE 2.4e10  
#define L3CACHE_SIZE 1.2e7
#define MIN_ITER 500  
#define TIME_LIMIT 3.0  
#define NUM_FORMATS 7

//#define PRINT_FEATURES
#define KERNEL_TAGS "./kernel_tags"
#define ESTIMATE_INFO "./results/estimate_info"

