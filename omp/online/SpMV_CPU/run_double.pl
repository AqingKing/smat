#!/usr/bin/perl

$matrices = "../../../mats_list/test_one_mat";
$out = "results";
#$matrices = "../../../mats_list/learn_test/test_mats_list";
#$out = "../Estimate_results_double";

$line_num = 0;

print "Using $matrices input MM matrices\n";
open (MM_MAT, $matrices) || die "Cannot open $matrices : $!\n";
while ($mm_file = <MM_MAT>)
{
    $line_num ++;
    chomp $mm_file;
    print $mm_file, "\n";
    @name = split ( /\//, $mm_file);  
    @tmp = split (/.mtx/, $name[-1]);
    mkdir "$out\/$name[5]";
    mkdir "$out\/$name[5]\/$name[6]";
    mkdir "$out\/$name[5]\/$name[6]\/$tmp[0]_perf";

    print "Generate new \"config.h\"\n";
    open (CONFIG_FILE, "config_init.h") || die "Cannot open \"config_init.h\" : $!\n";
    @config_init = <CONFIG_FILE>; 
    $config_num = @config_init;
    $config_init[$config_num] = "//output file names\n";
    $config_init[$config_num+1] = "#define KERNEL_TAGS \".\/kernel_tags_double\"\n";
    $config_init[$config_num+2] = "#define ESTIMATE_INFO \"$out\/$name[5]\/$name[6]\/$tmp[0]_perf\/estimate_info\"\n";
    open (WRITE_CONFIG, ">config.h") || die "Cannot write to \"config.h\": $!\n";
    print WRITE_CONFIG @config_init;
#    print @config_init;
    close (WRITE_CONFIG); 
    close (CONFIG_FILE);
    
    system ("make clean; make\n"); 
   
    print ("\nexport OMP_NUM_THREADS=2\n");
    system ("export OMP_NUM_THREADS=2\n"); 
    print "./spmv-icc --precision=64 ${mm_file} >
    $out\/$name[5]\/$name[6]\/$tmp[0]_perf\/running_log\n";
    system ("./spmv-icc --precision=64 ${mm_file} > \\
      $out\/$name[5]\/$name[6]\/$tmp[0]_perf\/running_log\n");
    print ("rm config.h\n\n");
    system ("rm config.h\n");
}
close(MM_MAT);

