#!/usr/bin/perl

$DB_rules_file = "../../../serial/offline/data_mining/SMAT_float.rules";
$DB_output_file = "../../../serial/offline/data_mining/output_float.txt";
$out = ">./rules_float/confidence";
$out_all = ">./rules_float/all_rules";
$out_dia = ">./rules_float/DIA_rules";
$out_ell = ">./rules_float/ELL_rules";
$out_csr = ">./rules_float/CSR_rules";
$out_coo = ">./rules_float/COO_rules";

$line_count = 0;
$ALl_rules_count = 0;
$DIA_rules_count = 0;
$ELL_rules_count = 0;
$CSR_rules_count = 0;
$COO_rules_count = 0;
$type_count = 0;
$rule_line_num = 0;
$test_case_num = 0;
$tag = 0;
@confidence = (0..0);
$confidence_num = 0;

#Generate the needed number of rules and their confidence
print "Using $DB_output_file input the output file of C5.0\n";
open (OUT_RULES, $DB_output_file) || die "Cannot open $DB_output_file : $!\n";
while ($line = <OUT_RULES>)
{
    chomp $line;
    @words_0 = split (/ /, $line);

#    printf ("%s\n",$words_0[0]);
    if ($words_0[0] eq "\tRule" )
    {
#      printf ("%s\n", $line);
      @words_1 = split (/'/, $line);
      @words_2 = split ( /\//, $words_1[0]);
      $total_rules_num = $words_2[-1];
      last;
    }
}
close(OUT_RULES);
printf ("Total rules number : %d\n", $total_rules_num);

open (OUT_RULES, $DB_output_file) || die "Cannot open $DB_output_file : $!\n";
while ($line = <OUT_RULES>)
{
    chomp $line;
    @words_0 = split (/ /, $line);

    if ( $words_0[0] eq "Evaluation" && $words_0[2] eq "test" )
    {
      @words_2 = split ( /\(/, $line);
      @words_3 = split (/ /, $words_2[1]);
      $test_case_num = $words_3[0];
printf ("test_case_num : %d\n", $test_case_num);
    }

    if ( $test_case_num != 0)
    {
      if ( $words_0[-1] eq "<<" )
      {
#      printf ("%s\n", $line);
        @words_2 = split ( /\(/, $line);
        @words_3 = split (/ /, $words_2[0]);
        $min_err_num = $words_3[-1];
        $min_err_ratio = $min_err_num / $test_case_num;
#printf ("min_err_num : %d\n", $min_err_num);
  printf ("min_err_ratio : %f\n", $min_err_ratio);
      }

      @words_1 = split (/-/, $line);
      if ($words_1[0] eq "\t1")
      {
#      printf ("%s\n", $line);
        @words_2 = split ( /\(/, $line);
        @words_3 = split (/ /, $words_2[0]);
        $err_num = $words_3[-1];
        $err_ratio = $err_num / $test_case_num;
#printf ("err_num : %d\n", $err_num);
        @words_3 = split (/ /, $words_1[1]);
        $rule_num = $words_3[0];
#  printf ("Rule %d : %f\n", $rule_num, $err_ratio);
#  printf ("Gap : %f\n", abs($err_ratio - $min_err_ratio) );

        if ( $tag == 0 && abs ($err_ratio - $min_err_ratio) < 0.01 )
        {
          $tag = 1;
          @words_2 = split (/ /, $words_1[1]);
          $part_rule_num = $words_2[0];
printf ("Rule %d : %f\n", $rule_num, $err_ratio);
        }
      }
    }
  }
close(OUT_RULES);
printf ("Part rules number : %d\n", $part_rule_num);

open (OUT, $out) || die "Cannot open $out : $!\n";

printf OUT ("Needed Rule number : %d\n", $part_rule_num);
open (OUT_RULES, $DB_output_file) || die "Cannot open $DB_output_file : $!\n";
while ($line = <OUT_RULES>)
{
    chomp $line;
    @words_0 = split (/ /, $line);

    if ( $words_0[0] eq "Rule" && $words_0[1] ne "utility" )
    {
#      printf ("2: %s\n", $line);
      @words_1 = split (/:/, $words_0[1]);
      $rule_num = $words_1[0];
    }
    
    @words_0 = split (/ /, $line);
#    printf ("%s\n", $words_0[0]);
    if ( $words_0[0] eq "\t->" )
    {
#      printf ("1: %s\n", $line);
      @words_1 = split (/\[/, $line);
      @words_2 = split (/\]/, $words_1[1]);
      $confidence[$confidence_num] = $words_2[0];
      $confidence_num ++;
    }
}
for $i (@confidence)
{
  printf OUT ("%f\n", $i);
}
close(OUT_RULES);
close(OUT);

##########################################################
#Generate four if_else files
open (OUT_DIA, $out_dia) || die "Cannot open $out_dia : $!\n";
open (OUT_ELL, $out_ell) || die "Cannot open $out_ell : $!\n";
open (OUT_CSR, $out_csr) || die "Cannot open $out_csr : $!\n";
open (OUT_COO, $out_coo) || die "Cannot open $out_coo : $!\n";
open (OUT_ALL, $out_all) || die "Cannot open $out_all : $!\n";

print "Using $DB_rules_file input the rules file of C5.0\n";
open (RULES, $DB_rules_file) || die "Cannot open $DB_rules_file : $!\n";
while ($line = <RULES>)
{
    chomp $line;
    @words_0 = split (/=/, $line);
    @words_1 = split (/\"/, $line);

    if ($words_0[0] eq "rules" )
    {
      $total_rule_num = $words_1[1];
      $default_format = $words_1[-1];
    }

    if ($words_0[0] eq "conds" )
    {
      $line_count ++;
#        printf OUT_DIA ("Rule %d:\n", $line_count);
    }
    if ( $line_count > $part_rule_num )
    {
      last;
    }

    #extract DIA rules
    if ($words_0[0] eq "conds" && $words_1[-1] eq "DIA" )
    {
      $DIA_rules_count ++;
      $type_num = $words_1[1];
      $class = $words_1[-1];
      $type_count = 0;
    }
    if ( $class eq "DIA" )
    {
      if ( $words_0[0] eq "type" )
      {
        $type_count ++;
        $type = $words_1[1];
        $att = $words_1[3];
        $cut = $words_1[5];
        $result = $words_1[7];
        if ( $result eq "<" )
        {
          $result = $result . "=";
        }

        if ( $type_count == 1 )
        {
          printf OUT_DIA ("if \( ");
        }
        if ( $type_count < $type_num )
        {
          printf OUT_DIA ("\( %s %s %f \) && ", $att, $result, $cut);
        }
        elsif ( $type_count == $type_num )
        {
          printf OUT_DIA ("\( %s %s %f \) \)\n", $att, $result, $cut);
          printf OUT_DIA ("\{\n");
          printf OUT_DIA ("\tformat = \"%s\";\n", $class);
          printf OUT_DIA ("\tconfidence = %f;\n", $confidence[$line_count-1]);
          printf OUT_DIA ("\tif \( max_confidence < confidence \)\n");
          printf OUT_DIA ("\t\tmax_confidence = confidence;\n");
          printf OUT_DIA ("\}\n");
        }
      }
    }
    #extract ELL rules
    if ($words_0[0] eq "conds" && $words_1[-1] eq "ELL" )
    {
      $ELL_rules_count ++;
      $type_num = $words_1[1];
      $class = $words_1[-1];
      $type_count = 0;
    }
    if ( $class eq "ELL" )
    {
      if ( $words_0[0] eq "type" )
      {
        $type_count ++;
        $type = $words_1[1];
        $att = $words_1[3];
        $cut = $words_1[5];
        $result = $words_1[7];
        if ( $result eq "<" )
        {
          $result = $result . "=";
        }

        if ( $type_count == 1 )
        {
          printf OUT_ELL ("if \( ");
        }
        if ( $type_count < $type_num )
        {
          printf OUT_ELL ("\( %s %s %f \) && ", $att, $result, $cut);
        }
        elsif ( $type_count == $type_num )
        {
          printf OUT_ELL ("\( %s %s %f \) \)\n", $att, $result, $cut);
          printf OUT_ELL ("\{\n");
          printf OUT_ELL ("\tformat = \"%s\";\n", $class);
          printf OUT_ELL ("\tconfidence = %f;\n", $confidence[$line_count-1]);
          printf OUT_ELL ("\tif \( max_confidence < confidence \)\n");
          printf OUT_ELL ("\t\tmax_confidence = confidence;\n");
          printf OUT_ELL ("\}\n");
        }
      }
    }
    #extract CSR rules
    if ($words_0[0] eq "conds" && $words_1[-1] eq "CSR" )
    {
      $CSR_rules_count ++;
      $type_num = $words_1[1];
      $class = $words_1[-1];
      $type_count = 0;
    }
    if ( $class eq "CSR" )
    {
      if ( $words_0[0] eq "type" )
      {
        $type_count ++;
        $type = $words_1[1];
        $att = $words_1[3];
        $cut = $words_1[5];
        $result = $words_1[7];
        if ( $result eq "<" )
        {
          $result = $result . "=";
        }

        if ( $type_count == 1 )
        {
          printf OUT_CSR ("if \( ");
        }
        if ( $type_count < $type_num )
        {
          printf OUT_CSR ("\( %s %s %f \) && ", $att, $result, $cut);
        }
        elsif ( $type_count == $type_num )
        {
          printf OUT_CSR ("\( %s %s %f \) \)\n", $att, $result, $cut);
          printf OUT_CSR ("\{\n");
          printf OUT_CSR ("\tformat = \"%s\";\n", $class);
          printf OUT_CSR ("\tconfidence = %f;\n", $confidence[$line_count-1]);
          printf OUT_CSR ("\tif \( max_confidence < confidence \)\n");
          printf OUT_CSR ("\t\tmax_confidence = confidence;\n");
          printf OUT_CSR ("\}\n");
        }
      }
    }
    #extract COO rules
    if ($words_0[0] eq "conds" && $words_1[-1] eq "COO" )
    {
      $COO_rules_count ++;
      $type_num = $words_1[1];
      $class = $words_1[-1];
      $type_count = 0;
    }
    if ( $class eq "COO" )
    {
      if ( $words_0[0] eq "type" )
      {
        $type_count ++;
        $type = $words_1[1];
        $att = $words_1[3];
        $cut = $words_1[5];
        $result = $words_1[7];
        if ( $result eq "<" )
        {
          $result = $result . "=";
        }

        if ( $type_count == 1 )
        {
          printf OUT_COO ("if \( ");
        }
        if ( $type_count < $type_num )
        {
          printf OUT_COO ("\( %s %s %f \) && ", $att, $result, $cut);
        }
        elsif ( $type_count == $type_num )
        {
          printf OUT_COO ("\( %s %s %f \) \)\n", $att, $result, $cut);
          printf OUT_COO ("\{\n");
          printf OUT_COO ("\tformat = \"%s\";\n", $class);
          printf OUT_COO ("\tconfidence = %f;\n", $confidence[$line_count-1]);
          printf OUT_COO ("\tif \( max_confidence < confidence \)\n");
          printf OUT_COO ("\t\tmax_confidence = confidence;\n");
          printf OUT_COO ("\}\n");
        }
      }
    }
    

=pod
    elsif ( $part_rule_num == $total_rule_num )  #The other situation 
    {
      if ($words_0[0] eq "conds")
      {
        $line_count ++;
        if ( $line_count == 1)
        {
          printf OUT_DIA ("if \( ");
        }
        elsif ( $line_count <= $part_rule_num )
        {
          printf OUT_DIA (" \)\n");
          printf OUT_DIA ("\{\n");
          printf OUT_DIA ("\tformat = \"%s\";\n", $class);
          printf OUT_DIA ("\tconfidence = \"%f\";\n", $confidence[$line_num-1]);
          printf OUT_DIA ("\}\n");
          printf OUT_DIA ("if \( ");
        }
        $type_num = $words_1[1];
        $class = $words_1[-1];
      }
      if ( $words_0[0] eq "type" )
      {
        $type_count ++;
        $type = $words_1[1];
        $att = $words_1[3];
        $cut = $words_1[5];
        $result = $words_1[7];
        if ( $result eq "<" )
        {
          $result = $result . "=";
        }
        if ( $type_count < $type_num )
        {
          printf OUT_DIA ("\( %s %s %f \) && ", $att, $result, $cut);
        }
        elsif ( $type_count == $type_num )
        {
          printf OUT_DIA ("\( %s %s %f \) ", $att, $result, $cut);
        }
      }
      printf OUT_DIA (" \)\n");
      printf OUT_DIA ("\{\n");
      printf OUT_DIA ("\tformat = \"%s\";\n", $class);
      printf OUT_DIA ("\tconfidence = \"%f\";\n", $confidence[$line_num-1]);
      printf OUT_DIA ("\}\n");
    }
=cut

}
print "Using $DB_rules_file input the rules file of C5.0\n";
open (RULES, $DB_rules_file) || die "Cannot open $DB_rules_file : $!\n";
while ($line = <RULES>)
{
    chomp $line;
    @words_0 = split (/=/, $line);
    @words_1 = split (/\"/, $line);
    $line_count = 0;

    if ($words_0[0] eq "conds" )
    {
      $line_count ++;
#        printf OUT_DIA ("Rule %d:\n", $line_count);
    }
    if ( $line_count > $part_rule_num )
    {
      last;
    }

    #extract all rules
    if ($words_0[0] eq "conds" )
    {
      $ALL_rules_count ++;
      $type_num = $words_1[1];
      $class = $words_1[-1];
      $type_count = 0;
    }
      if ( $words_0[0] eq "type" )
      {
        $type_count ++;
        $type = $words_1[1];
        $att = $words_1[3];
        $cut = $words_1[5];
        $result = $words_1[7];
        if ( $result eq "<" )
        {
          $result = $result . "=";
        }

        if ( $type_count == 1 )
        {
          printf OUT_ALL ("if \( ");
        }
        if ( $type_count < $type_num )
        {
          printf OUT_ALL ("\( %s %s %f \) && ", $att, $result, $cut);
        }
        elsif ( $type_count == $type_num )
        {
          printf OUT_ALL ("\( %s %s %f \) \)\n", $att, $result, $cut);
          printf OUT_ALL ("\{\n");
          printf OUT_ALL ("\tformat = \"%s\";\n", $class);
          printf OUT_ALL ("\tconfidence = %f;\n", $confidence[$line_count-1]);
          printf OUT_ALL ("\tif \( max_confidence < confidence \)\n");
          printf OUT_ALL ("\t\tmax_confidence = confidence;\n");
          printf OUT_ALL ("\}\n");
        }
      }
}

close(RULES);
printf OUT_DIA ("Rules number : %d\n", $DIA_rules_count);
printf OUT_ELL ("Rules number : %d\n", $ELL_rules_count);
printf OUT_CSR ("Rules number : %d\n", $CSR_rules_count);
printf OUT_COO ("Rules number : %d\n", $COO_rules_count);
printf OUT_ALL ("Rules number : %d\n", $ALL_rules_count);

close(OUT_DIA);
close(OUT_ELL);
close(OUT_CSR);
close(OUT_COO);
close(OUT_ALL);
