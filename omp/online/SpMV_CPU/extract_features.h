#include <stdio.h>
#include <iostream>
#include <assert.h>
#include <math.h>
#include "config.h"
#include "timer.h"

template <typename IndexType, typename ValueType>
int extract_features (int argc, char **argv, csr_matrix<IndexType,ValueType>& csr)
{
#ifdef TIMING
    long long int Tstart, Tend;
#endif
    int flag_return = 0;
    IndexType M = csr.num_rows;
    IndexType N = csr.num_cols;
    IndexType NNZ = csr.num_nonzeros;
    double aver_RD = (double)NNZ / M;
    IndexType Ndiags = 0, NTdiags = 0;
    double NTdiags_ratio = 0;
    double ER_DIA = 0;
    IndexType max_RD = 0;
    IndexType min_RD = 0;
    double dev_RD = 0;
    double ER_ELL = 0;
    double R = 0, C = 0;
    double confidence = 0;

    FILE *fp;
    double dia_perf = 0, ell_perf = 0, csr_perf = 0, coo_perf = 0;
    double max_perf = 0;
    int result = 0;
    IndexType total_count = 0, count = 0;

    //Extract the matrix feature 
#ifdef TIMING
    timer total_time_struct;
#endif

    //Determine DIA and ELL format
    printf("\nDIA features: \n");
#ifdef TIMING
        timer DIA_time_struct;
#endif
        IndexType *diag_map = new_array<IndexType>(num_rows + num_cols);
        std::fill(diag_map, diag_map + num_rows + num_cols, 0);
        IndexType *nzs_per_row = new_array<IndexType>(num_rows);
        min_RD = csr.Ap[1] - csr.Ap[0];
        max_RD = 0; 
        dev_RD = (csr.Ap[1]-csr.Ap[0]-aver_RD) * (csr.Ap[1]-csr.Ap[0]-aver_RD);
        IndexType row_degree;

        for(IndexType i = 0; i < num_rows; i++)
        {
            for(IndexType jj = csr.Ap[i]; jj < csr.Ap[i+1]; jj++)
            {
                IndexType j = csr.Aj[jj];
                IndexType map_index = (num_rows - i) + j; 
                if(diag_map[map_index] == 0)
                    Ndiags ++;
                diag_map[map_index] ++;
            }
            row_degree = csr.Ap[i+1] - csr.Ap[i];
            nzs_per_row[i] = row_degree;
            if (max_RD < row_degree)
                max_RD = row_degree;
            if (min_RD > row_degree)
                min_RD = row_degree;
            dev_RD += (row_degree - aver_RD) * (row_degree - aver_RD);
        }
        ER_DIA = (double)csr.num_nonzeros / (Ndiags * num_rows);
        dev_RD /= num_rows;
        ER_RD = (double)num_nonzeros / (num_rows * max_RD);

        IndexType j = 0;
        double ratio;
        for (IndexType i=0; i<num_cols+num_rows; i++)
        {
            if (diag_map[i] != 0)
            {
                j ++;
                ratio = (double)diag_map[i] /num_rows;
                if (ratio >= DIAG_RATIO_TH )
                    NTdiags ++;
            }
        }
        assert( j == Ndiags);
        NTdiags_ratio = (double)NTdiags/Ndiags;
        free (diag_map);
        
#ifdef TIMING
        double DIA_time = DIA_time_struct.milliseconds_elapsed();
#endif
/*
 * TODO : Insert rules for DIA
 */
#ifdef TIMING
        printf ("DIA search time: %8.4lf ms\n", DIA_time);
#endif

//Determine ELL format
/*
 * TODO : Insert rules for ELL
 */

    //Determine COO format
#ifdef TIMING
        timer COO_time_struct;
#endif
    double *nzs_distribution = new_array<double>(num_cols+1);
    std::fill(nzs_distribution, nzs_distribution + num_cols+1, 0.0);
    IndexType number;
    for(IndexType i = 0; i < num_rows; i++)
    {
        number = nzs_per_row[i];
        nzs_distribution[number] += 1;
    }
    
    total_count = 0;
    IndexType peak_pos = 0, peak_Rdegree = 0;
    double total_sum = 0, peak_ratio = 0;
    for(IndexType i = 1; i <= num_cols; i++)
    {
        if ( nzs_distribution[i] != 0.0)
        {
            nzs_distribution[i] = nzs_distribution[i] / (num_rows-nzs_distribution[0]);  
            total_count ++;
            total_sum += nzs_distribution[i];
            if ( peak_ratio < nzs_distribution[i] )
            {
              peak_ratio = nzs_distribution[i];
              peak_pos = total_count;
              peak_Rdegree = i;
            }
        }
    }
    printf("\tDifferent nzs_per_row count: %d\n", total_count);
    printf("\tTotal sum (should be 1): %f\n", total_sum);
    printf("\tPeak ratio: %f,  corresponded row degree: %d\n", peak_ratio, peak_Rdegree);

    count = 0;
    double aver_x = 0, aver_y = 0;
    for(IndexType i = peak_Rdegree; i <= num_cols; i++)
    {
        if ( nzs_distribution[i] != 0.0)
        {
            count ++;
            aver_x += log10(i);
            aver_y += log10(nzs_distribution[i]);
        }
    }
    assert ( count == (total_count - peak_pos + 1) );
    printf("\tUsed count: %d\n", count);
    aver_x /= count;
    aver_y /= count;
    printf("aver_x: %.2lf, aver_y: %.2lf\n", aver_x, aver_y);

    double a_up = 0, a_down = 0;
    double a = 0, b = 0, r, c;
    for(IndexType i = peak_Rdegree; i <= num_cols; i++)
    {
        if ( nzs_distribution[i] != 0.0)
        {
            a_up += (log10(i) - aver_x) * (log10(nzs_distribution[i]) - aver_y);
            a_down += (log10(i) - aver_x) * (log10(i) - aver_x);
        }
    }
    a = a_up / a_down;
    b = aver_y - a*aver_x;
    R = 0 - a;
    C = pow(10, b);
#ifdef TIMING
        double COO_time = COO_time_struct.milliseconds_elapsed();
#endif

    double test_sum = 0;
    for(IndexType i = 1; i < peak_Rdegree; i++)
        if ( nzs_distribution[i] != 0.0)
        {
            test_sum += nzs_distribution[i] ;
        }
    printf("\tpart test_sum: %.2lf\n", test_sum);
    for(IndexType i = peak_Rdegree; i <= num_cols; i++)
        if ( nzs_distribution[i] != 0.0)
        {
            test_sum += ( c* pow(i, 0-r) );
        }
    
    printf("\ttest_sum: %.2lf (should similar to 1)\n", test_sum);
    printf("\tr: %.2lf, c: %lf\n", r, c);
    printf("\tr should be in (0,4)\n");
    
    free(nzs_distribution);
    free (nzs_per_row); 

    printf("\nWhether to use COO: \n");
    /*
     * TODO : Insert rules for COO
     */
#ifdef TIMING
        printf ("COO search time: %8.4lf ms\n", COO_time);
#endif

#ifdef TIMING
    double total_time = total_time_struct.milliseconds_elapsed();
    printf("\nTotal search time: %8.4lf ms\n", total_time);
#endif

    return result;  
}
