/*
 *  Copyright 2008-2009 NVIDIA Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */


#include <iostream>
#include <stdio.h>
#include <omp.h>
#include "cmdline.h"
#include "gallery.h"
#include "config.h"
#include "timer.h"

#include "tests.h"
#include "format_estimate.h"
#include "run_once_for_estimate.h"

void usage(int argc, char** argv)
{
    std::cout << "Usage:\n";
    std::cout << "\t" << argv[0] << " with following parameters:\n";
    std::cout << "\t" << " my_matrix.mtx\n";
    std::cout << "\t" << " --precision=32(or 64)\n";
    std::cout << "\t" << " --max_diags_limit=20\n";
    std::cout << "\t" << " --dia_fill_ratio=0.95\n";
    std::cout << "\t" << " --min_diags_ratio=0.1\n";
    std::cout << "\t" << " --ell_max_cols=100\n";
    std::cout << "\t" << " --ell_min_deviation=1\n";
    std::cout << "Note: my_matrix.mtx must be real-valued sparse matrix in the MatrixMarket file format.\n"; 
}


template <typename IndexType, typename ValueType>
void run_all_kernels(int argc, char **argv)
{
    char * mm_filename = NULL;
    for(int i = 1; i < argc; i++){
        if(argv[i][0] != '-'){
            mm_filename = argv[i];
            break;
        }
    }

    csr_matrix<IndexType,ValueType> csr;
    
    if (mm_filename == NULL)
    {
        printf("No input MM file!\n");
        return;
    }
    else
        csr = read_csr_matrix<IndexType,ValueType>(mm_filename);
    printf("Using %d-by-%d matrix with %d nonzero values\n", csr.num_rows, csr.num_cols, csr.num_nonzeros); 

#ifdef TIMING
//print in CSR format
    printf("Writing matrix in CSR format to test_CSR ...\n");
    FILE *fp = fopen("test_CSR", "w");
    fprintf(fp, "%d\t%d\t%d\n", csr.num_rows, csr.num_cols, csr.num_nonzeros);
    fprintf(fp, "csr.Ap:\n");
    for (IndexType i=0; i<csr.num_rows+1; i++)
    {
      fprintf(fp, "%d\n", csr.Ap[i]);
    }
    fprintf(fp, "csr.Aj:\n");
    for (IndexType i=0; i<csr.num_nonzeros; i++)
    {
      fprintf(fp, "%d\n", csr.Aj[i]);
    }
    fprintf(fp, "csr.Ax:\n");
    for (IndexType i=0; i<csr.num_nonzeros; i++)
    {
      fprintf(fp, "%lf\n", csr.Ax[i]);
    }
    fclose(fp);
#endif 

    // fill matrix with random values: some matrices have extreme values, 
    // which makes correctness testing difficult, especially in single precision
    
    srand(13);
    for(IndexType i = 0; i < csr.num_nonzeros; i++){
        csr.Ax[i] = 1.0 - 2.0 * (rand() / (RAND_MAX + 1.0)); 
    }
    
    printf("\nfile=%s rows=%d cols=%d nonzeros=%d\n", mm_filename, csr.num_rows, csr.num_cols, csr.num_nonzeros);

    //OMP section
    int nthreads = 0, dynamic_value = 0, tid;
#pragma omp parallel
    {
      tid = omp_get_thread_num();
      if ( tid == 0 )
        nthreads = omp_get_num_threads();
      printf("Thread : %d\n", tid);
    }
      printf("num_thread : %d\n", nthreads);
    
    //divide matrix by nonzeros and rows
    IndexType *block_lens_rows, *block_lens_nnzs, *block_Ap_start, *block_Ap_end, *block_Ax;
    block_lens_rows = (IndexType *) malloc(sizeof(IndexType)*nthreads);
    memset(block_lens_rows, 0, sizeof(IndexType)*nthreads);
    block_lens_nnzs = (IndexType *) malloc(sizeof(IndexType)*nthreads);
    memset(block_lens_nnzs, 0, sizeof(IndexType)*nthreads);
    block_Ap_start = (IndexType *) malloc(sizeof(IndexType)*nthreads);
    memset(block_Ap_start, 0, sizeof(IndexType)*nthreads);
    block_Ap_end = (IndexType *) malloc(sizeof(IndexType)*nthreads);
    memset(block_Ap_end, 0, sizeof(IndexType)*nthreads);
    block_Ax = (IndexType *) malloc(sizeof(IndexType)*nthreads);
    memset(block_Ax, 0, sizeof(IndexType)*nthreads);

    if (nthreads > 1)
    {
      if (csr.num_nonzeros > nthreads)
      {
        //Suit for 0-based array
        int aver_BL = csr.num_nonzeros / nthreads;
        IndexType rest = csr.num_nonzeros;
        int count_BL = 0;
        int dis1 = 0, dis2 = 0;
        IndexType sum1 = 0, sum2 = 0;
        IndexType start_row = 0;

        for (IndexType i=0; i<csr.num_rows; i++)
        {
          sum1 = ( csr.Ap[i] - csr.Ap[start_row] );  //Row i-1
          sum2 = ( csr.Ap[i+1] - csr.Ap[start_row] );  //Row i
          if (sum1 <= aver_BL && sum2 >= aver_BL )
          {
              dis1 = aver_BL - sum1;
              dis2 = sum2 - aver_BL;
              if (dis1 < dis2)
              {
                block_lens_rows[count_BL] = i - start_row;  //i rows
                block_lens_nnzs[count_BL] = sum1;  //i rows
                block_Ap_start[count_BL+1] = i;
                block_Ap_end[count_BL] = i;
                block_Ax[count_BL+1] = block_Ax[count_BL] + sum1;
                start_row = i;
                rest -= sum1;
                sum1 = ( csr.Ap[i] - csr.Ap[start_row] );
                sum2 = ( csr.Ap[i+1] - csr.Ap[start_row] );
              }
              else if (dis1 >= dis2)
              {
                block_lens_rows[count_BL] = i+1-start_row;
                block_lens_nnzs[count_BL] = sum2;
                block_Ap_start[count_BL+1] = i+1;
                block_Ap_end[count_BL] = i+1;
                block_Ax[count_BL+1] = block_Ax[count_BL] + sum2;
                start_row = i+1;
                rest -= sum2;
              }

              count_BL ++;
              if (count_BL == nthreads-1 )
              {
                block_lens_rows[count_BL] = csr.num_rows - start_row;
                block_lens_nnzs[count_BL] = rest;
                block_Ap_end[count_BL] = csr.num_rows;
                break;
              }
              aver_BL = rest / (nthreads-count_BL);
          }
        }
      }
      else
      {
        printf("Small matrix. Only 1 thread is needed!\n");
        omp_set_num_threads(1);
        printf("Change num_threads to %d\n", omp_get_num_threads());
      }
    }

#if 0
    printf("block_lens_rows:\n");
    for ( int i=0; i<nthreads; i++)
    {
        printf("%d\t", block_lens_rows[i]);
    }
    printf("\nblock_Ap_start:\n");
    for ( int i=0; i<nthreads; i++)
    {
        printf("%d\t", block_Ap_start[i]);
    }
    printf("\nblock_Ap_end:\n");
    for ( int i=0; i<nthreads; i++)
    {
        printf("%d\t", block_Ap_end[i]);
    }
    printf("\nblock_lens_nnzs:\n");
    for ( int i=0; i<nthreads; i++)
    {
        printf("%d\t", block_lens_nnzs[i]);
    }
    printf("\nblock_Ax:\n");
    for ( int i=0; i<nthreads; i++)
    {
        printf("%d\t", block_Ax[i]);
    }
    printf("\n");
    fflush(stdout);
#endif
    

    //Shared arrays and variables
    int kernel_tags[4] = {0, 0, 0, 0};
    FILE *fp_tag;
    fp_tag = fopen(KERNEL_TAGS, "r");
    fscanf(fp_tag, "%d", &kernel_tags[0]); 
    fscanf(fp_tag, "%d", &kernel_tags[1]);
    fscanf(fp_tag, "%d", &kernel_tags[2]);
    fscanf(fp_tag, "%d", &kernel_tags[3]);
    fclose(fp_tag);

    int estimate_tags[nthreads];
    double estimate_dia_msec[nthreads], estimate_ell_msec[nthreads], estimate_csr_msec[nthreads], estimate_coo_msec[nthreads]; //All the 4 arrays have "nthreads" nonzero time

    double actual_dia_msec[nthreads], actual_ell_msec[nthreads], actual_csr_msec[nthreads], actual_coo_msec[nthreads]; //Each of the 4 arrays has "nthreads" nonzero time
    double actual_dia_max_msec = 0, actual_ell_max_msec = 0, actual_csr_max_msec = 0, actual_coo_max_msec = 0;
    double actual_dia_max_gflops = 0, actual_ell_max_gflops = 0, actual_csr_max_gflops = 0, actual_coo_max_gflops = 0;
    int actual_tag = 0;

    double estimate_max_msec = 0;
    double estimate_max_gflops = 0, actual_max_gflops = 0;
    char *estimate_best_format_combine, *estimate_best_format, *actual_best_format;
    estimate_best_format_combine = (char*)malloc(5*nthreads*sizeof(char));
    estimate_best_format = (char*)malloc(5*sizeof(char));
    actual_best_format = (char*)malloc(5*sizeof(char));

    //tmp data structures
    double dia_gflops[nthreads], ell_gflops[nthreads], csr_gflops[nthreads], coo_gflops[nthreads];

#pragma omp parallel private(tid) default(shared) 
    {
      tid = omp_get_thread_num();
      //divide CSR data structure
      csr_matrix<IndexType,ValueType> block_csr;  //Private
      if ( nthreads > 1)
      {
        IndexType* block_CSR_Ap = new_array<IndexType>(block_lens_rows[tid] + 1);  
        IndexType* block_CSR_Aj = &csr.Aj[block_Ax[tid]];
        ValueType* block_CSR_Ax = &csr.Ax[block_Ax[tid]];
        
        for ( IndexType i=0; i<block_lens_rows[tid] + 1; i++ )
        {
          IndexType block_start = block_Ap_start[tid];
          block_CSR_Ap[i] = csr.Ap[i + block_start] - block_Ax[tid];
        }
#pragma omp barrier
#if 0
        for ( IndexType i=0; i<block_lens_nnzs[tid]; i++ )
        {
          IndexType block_start = block_Ax[tid];
          block_CSR_Aj[i] = csr.Aj[i + block_start]; //conflict
          block_CSR_Ax[i] = csr.Ax[i + block_start]; //conflict
        }
#pragma omp barrier
#endif
        block_csr.num_rows = block_lens_rows[tid];
        block_csr.num_cols = csr.num_cols;
        block_csr.num_nonzeros = block_lens_nnzs[tid];
        block_csr.Ap = block_CSR_Ap;
        block_csr.Aj = block_CSR_Aj;
        block_csr.Ax = block_CSR_Ax;
      }
      else
      {
        block_csr.num_rows = csr.num_rows;
        block_csr.num_cols = csr.num_cols;
        block_csr.num_nonzeros = csr.num_nonzeros;
        block_csr.Ap = csr.Ap;
        block_csr.Aj = csr.Aj;
        block_csr.Ax = csr.Ax;
      }
      printf ("\n");
#if 0
      printf ("Thread %d :\n", tid);
      printf ("num_rows : %d\n", block_csr.num_rows);
      printf ("num_cols : %d\n", block_csr.num_cols);
      printf ("num_nonzeros : %d\n", block_csr.num_nonzeros);
      printf ("Ap : %d\n", block_csr.Ap[0]);
      printf ("Aj : %d\n", block_csr.Aj[0]);
      printf ("Ax : %lf\n", block_csr.Ax[0]);
      printf ("\n");
      fflush(stdout);
#endif

    //Input the best kernel for each format

    printf("[T %d] : dia_kernel_tag : %d\n", tid, kernel_tags[0] );
    printf("[T %d] : ell_kernel_tag : %d\n", tid, kernel_tags[1] );
    printf("[T %d] : csr_kernel_tag : %d\n", tid, kernel_tags[2] );
    printf("[T %d] : coo_kernel_tag : %d\n", tid, kernel_tags[3] );

#ifdef TESTING
  //print in CSR format
    if ( tid == 0 )
    {
    printf("[T 0] : Writing matrix in CSR format to test_CSR ...\n");
    FILE *fp = fopen("test_CSR_T0", "w");
    fprintf(fp, "%d\t%d\t%d\n", block_csr.num_rows, block_csr.num_cols, block_csr.num_nonzeros);
    fprintf(fp, "block_csr.Ap:\n");
    for (IndexType i=0; i<block_csr.num_rows+1; i++)
    {
      fprintf(fp, "%d\n", block_csr.Ap[i]);
    }
    fprintf(fp, "block_csr.Aj:\n");
    for (IndexType i=0; i<block_csr.num_nonzeros; i++)
    {
      fprintf(fp, "%d\n", block_csr.Aj[i]);
    }
    fprintf(fp, "block_csr.Ax:\n");
    for (IndexType i=0; i<block_csr.num_nonzeros; i++)
    {
      fprintf(fp, "%lf\n", block_csr.Ax[i]);
    }
    fclose(fp);
    }
    if ( tid == 1 )
    {
    printf("[T 1] : Writing matrix in CSR format to test_CSR ...\n");
    FILE *fp = fopen("test_CSR_T1", "w");
    fprintf(fp, "%d\t%d\t%d\n", block_csr.num_rows, block_csr.num_cols, block_csr.num_nonzeros);
    fprintf(fp, "block_csr.Ap:\n");
    for (IndexType i=0; i<block_csr.num_rows+1; i++)
    {
      fprintf(fp, "%d\n", block_csr.Ap[i]);
    }
    fprintf(fp, "block_csr.Aj:\n");
    for (IndexType i=0; i<block_csr.num_nonzeros; i++)
    {
      fprintf(fp, "%d\n", block_csr.Aj[i]);
    }
    fprintf(fp, "block_csr.Ax:\n");
    for (IndexType i=0; i<block_csr.num_nonzeros; i++)
    {
      fprintf(fp, "%lf\n", block_csr.Ax[i]);
    }
    fclose(fp);
    }
#endif 

    estimate_dia_msec[tid] = 0;
    estimate_ell_msec[tid] = 0;
    estimate_csr_msec[tid] = 0;
    estimate_coo_msec[tid] = 0;
    actual_dia_msec[tid] = 0;
    actual_ell_msec[tid] = 0;
    actual_csr_msec[tid] = 0;
    actual_coo_msec[tid] = 0;
    estimate_tags[tid] = 0;
#pragma omp barrier

    //Estimate the best format
      timer estimate_time_struct;
      format_estimate ( block_csr);
      printf ("[T %d] : tag after format_estimate : %d\n", tid, block_csr.tag);
      if (block_csr.tag == 0)
        block_csr.tag = run_once_for_estimate ( block_csr, kernel_tags[0], kernel_tags[1], kernel_tags[2], kernel_tags[3] );
      double estimate_time = estimate_time_struct.milliseconds_elapsed();
      printf ("[T %d] : tag after run once : %d\n", tid, block_csr.tag);
      printf ("\n[T %d] : Estimate time: %8.4lf ms\n", tid, estimate_time);

      estimate_tags[tid] = block_csr.tag;
#pragma omp barrier

      if ( block_csr.tag == 1 )
      {
        test_dia_matrix_kernels(block_csr, kernel_tags[0], &dia_gflops[tid], &estimate_dia_msec[tid]);
#pragma omp barrier
      }
      else if ( block_csr.tag == 2 )
      {
        test_ell_matrix_kernels(block_csr, kernel_tags[1], &ell_gflops[tid], &estimate_ell_msec[tid]);
#pragma omp barrier
      }
      else if ( block_csr.tag == 3 )
      {
        test_csr_matrix_kernels(block_csr, kernel_tags[2], &csr_gflops[tid], &estimate_csr_msec[tid]);
#pragma omp barrier
      }
      else if ( block_csr.tag == 4 )
      {
        test_coo_matrix_kernels(block_csr, kernel_tags[3], &coo_gflops[tid], &estimate_coo_msec[tid]);
#pragma omp barrier
      }

      //Run all the formats to find the actual best format
//      printf ("\nTest all the four formats performance:\n");
      test_dia_matrix_kernels(block_csr, kernel_tags[0], &dia_gflops[tid], &actual_dia_msec[tid]);
#pragma omp barrier
      fflush(stdout);
      test_ell_matrix_kernels(block_csr, kernel_tags[1], &ell_gflops[tid], &actual_ell_msec[tid]);
#pragma omp barrier
      fflush(stdout);
      test_csr_matrix_kernels(block_csr, kernel_tags[2], &csr_gflops[tid], &actual_csr_msec[tid]);
#pragma omp barrier
      fflush(stdout);
      test_coo_matrix_kernels(block_csr, kernel_tags[3], &coo_gflops[tid], &actual_coo_msec[tid]);
#pragma omp barrier
      fflush(stdout);

      // Find the maximum time to calculate the performance
      if ( tid == 0 )
      {
        //Estimate
        for ( int i=0; i<nthreads; i++ )
        {
          if ( estimate_tags[i] == 1 )
          {
            if ( estimate_max_msec < estimate_dia_msec[i] )
              estimate_max_msec = estimate_dia_msec[i];
            if ( i == 0 )
              strcpy ( estimate_best_format_combine, "DIA");
            else
              estimate_best_format_combine = strcat(estimate_best_format_combine, ", DIA");
          }
          else if ( estimate_tags[i] == 2 )
          {
            if ( estimate_max_msec < estimate_ell_msec[i] )
              estimate_max_msec = estimate_ell_msec[i];
            if ( i == 0 )
              strcpy ( estimate_best_format_combine, "ELL");
            else
              estimate_best_format_combine = strcat(estimate_best_format_combine, ", ELL");
          }
          else if ( estimate_tags[i] == 3 )
          {
            if ( estimate_max_msec < estimate_csr_msec[i] )
              estimate_max_msec = estimate_csr_msec[i];
            if ( i == 0 )
              strcpy ( estimate_best_format_combine, "CSR");
            else
              estimate_best_format_combine = strcat(estimate_best_format_combine, ", CSR");
          }
          else if ( estimate_tags[i] == 4 )
          {
            if ( estimate_max_msec < estimate_coo_msec[i] )
              estimate_max_msec = estimate_coo_msec[i];
            if ( i == 0 )
              strcpy ( estimate_best_format_combine, "COO");
            else
              estimate_best_format_combine = strcat(estimate_best_format_combine, ", COO");
          }
        }
        printf ("Estimate_best_format : %s\n", estimate_best_format_combine);
        if (estimate_max_msec != 0 )
          estimate_max_gflops = (2.0 * (double) csr.num_nonzeros / estimate_max_msec) / 1e6;
       
#if 1
       //Actual 
        for ( int i=0; i<nthreads; i++ )
        {
          actual_dia_max_msec = std::max(actual_dia_max_msec, actual_dia_msec[i]);
          actual_ell_max_msec = std::max(actual_ell_max_msec, actual_ell_msec[i]);
          actual_csr_max_msec = std::max(actual_csr_max_msec, actual_csr_msec[i]);
          actual_coo_max_msec = std::max(actual_coo_max_msec, actual_coo_msec[i]);
        }
        if (actual_dia_max_msec != 0 )
          actual_dia_max_gflops = (2.0 * (double) csr.num_nonzeros / actual_dia_max_msec) / 1e6;
        if (actual_ell_max_msec != 0 )
          actual_ell_max_gflops = (2.0 * (double) csr.num_nonzeros / actual_ell_max_msec) / 1e6;
        if (actual_csr_max_msec != 0 )
          actual_csr_max_gflops = (2.0 * (double) csr.num_nonzeros / actual_csr_max_msec) / 1e6;
        if (actual_coo_max_msec != 0 )
          actual_coo_max_gflops = (2.0 * (double) csr.num_nonzeros / actual_coo_max_msec) / 1e6;


        if (actual_max_gflops < actual_dia_max_gflops)
        {
           actual_max_gflops = actual_dia_max_gflops;
           actual_best_format = "DIA";
           actual_tag = 1;
        }
        if (actual_max_gflops < actual_ell_max_gflops)
        {
           actual_max_gflops = actual_ell_max_gflops;
           actual_best_format = "ELL";
           actual_tag = 2;
        }
        if (actual_max_gflops < actual_csr_max_gflops)
        {
           actual_max_gflops = actual_csr_max_gflops;
           actual_best_format = "CSR";
           actual_tag = 3;
        }
        if (actual_max_gflops < actual_coo_max_gflops)
        {
           actual_max_gflops = actual_coo_max_gflops;
           actual_best_format = "COO";
           actual_tag = 4;
        }
        
        double perf_lost = (actual_max_gflops - estimate_max_gflops) / actual_max_gflops;
        if ( strcmp (actual_best_format, estimate_best_format_combine) )
          printf ("Wrong Predict! Performance lost : %.2f %%\n", 100*perf_lost);
        else
        {
          printf ("Right Predict!\n");
          estimate_max_gflops = actual_max_gflops;
        }

        printf ("\nEstimate format: %s, Performance : %.2lf GFLOPS\n", estimate_best_format_combine, estimate_max_gflops);
        printf("DIA performance : %.2lf\n", actual_dia_max_gflops);
        printf("ELl performance : %.2lf\n", actual_ell_max_gflops);
        printf("CSR performance : %.2lf\n", actual_csr_max_gflops);
        printf("COO performance : %.2lf\n", actual_coo_max_gflops);
        printf ("Actual best format : %s , Performance : %.2lf GFLOPS\n", actual_best_format, actual_max_gflops);
        fflush(stdout);
#endif
      }

      if (nthreads > 1)
      {
        free(block_csr.Ap);
//        free(block_csr.Aj);
//        free(block_csr.Ax);
      }
    }  //End of OMP parallel

    FILE *fp_info = fopen(ESTIMATE_INFO, "w");
    fprintf (fp_info, "Estimate format: %s, Performance : %.2lf GFLOPS\n", estimate_best_format_combine, estimate_max_gflops);
    fprintf(fp_info, "DIA performance : %.2lf\n", actual_dia_max_gflops);
    fprintf(fp_info, "ELl performance : %.2lf\n", actual_ell_max_gflops);
    fprintf(fp_info, "CSR performance : %.2lf\n", actual_csr_max_gflops);
    fprintf(fp_info, "COO performance : %.2lf\n", actual_coo_max_gflops);
    fprintf (fp_info, "Actual best format : %s , Performance : %.2lf GFLOPS\n", actual_best_format, actual_max_gflops);

    free(block_lens_rows);
    free(block_lens_nnzs);
    free(block_Ap_start);
    free(block_Ap_end);
    free(block_Ax);
    delete_host_matrix(csr);
}

int main(int argc, char** argv)
{
    if (get_arg(argc, argv, "help") != NULL){
        usage(argc, argv);
        return EXIT_SUCCESS;
    }

    int precision = 32;
    char * precision_str = get_argval(argc, argv, "precision");
    if(precision_str != NULL)
        precision = atoi(precision_str);
    printf("Using %d-bit floating point precision\n", precision);

    if(precision ==  32)
        run_all_kernels<int, float>(argc,argv);
    else if(precision == 64)
        run_all_kernels<int, double>(argc,argv);
    else{
        usage(argc, argv);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

