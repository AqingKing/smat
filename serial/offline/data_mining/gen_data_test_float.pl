#!/usr/bin/perl

#$matrices = "../../../mats_list/test_one_mat";
#TEST
$matrices = "../../../mats_list/learn_test/test_mats_list";
$out = ">SMAT_float.test";
$perf_path = "../Mats_features_float";

$count = 0;

print "Using $matrices input MM matrices\n";
open(OUT, $out) || die "Cannot open $out : $!\n";

open (MM_MATS, $matrices) || die "Cannot open $matrices : $!\n";
while ($mm_file = <MM_MATS>)
{
#    print $mm_file;
    chomp $mm_file;
    @name = split ( /\//, $mm_file);  
    @tmp = split (/.mtx/, $name[-1]);

    open (MAT_FILE, $mm_file) || die "Cannot open $mm_file : $!\n";
    while ($line = <MAT_FILE>)
    {
      chomp $line;
      @words = split (/ /, $line);
      if ( $words[1] eq "id:" )
      {
        $ID = $words[2];
        last;
      }
    }
    close(MAT_FILE);

    $chars_file = $perf_path . "\/$name[5]\/$name[6]\/$tmp[0]_perf";
    $paras_file = $chars_file . "\/mat_features";
    open (PARAS_FILE, $paras_file) || die "Cannot open $paras_file : $!\n";
    while ($para = <PARAS_FILE>)
    {
        chomp $para;
        @eles = split ( /\ :\ /, $para);  
        @eles_2 = split (/ /, $para);  
        if ($eles[0] eq "M")
        {
          printf OUT ("$eles[1],");
        }
        if ($eles[0] eq "N")
        {
          printf OUT ("$eles[1],");
        }
        if ($eles[0] eq "NNZ")
        {
          printf OUT ("$eles[1],");
        }
        if ($eles[0] eq "Ndiags")
        {
          printf OUT ("$eles[1],");
        }
        if ($eles_2[0] eq "NTdiags_ratio")
        {
          printf OUT ("$eles_2[2],");
        }
        if ($eles[0] eq "ER_DIA")
        {
          if ( $eles[1] eq "inf" )
          {
            printf OUT ("0,");
          }
          else
          {
            printf OUT ("$eles[1],");
          }
        }
        if ($eles[0] eq "aver_RD")
        {
          printf OUT ("$eles[1],");
        }
        if ($eles[0] eq "max_RD")
        {
          printf OUT ("$eles[1],");
        }
        if ($eles[0] eq "min_RD")
        {
          printf OUT ("$eles[1],");
        }
        if ($eles[0] eq "dev_RD")
        {
          printf OUT ("$eles[1],");
        }
        if ($eles[0] eq "ER_RD")
        {
          printf OUT ("$eles[1],");
        }
        if ($eles[0] eq "row_bounce")
        {
          printf OUT ("$eles[1],");
        }
        if ($eles[0] eq "R")
        {
          if ( $eles[1] eq "-nan" || $eles[1] eq "nan" )
          {
            printf OUT ("N\/A,");
          }
          else
          {
            printf OUT ("$eles[1],");
          }
        }
        if ($eles[0] eq "Best_format")
        {
          printf OUT ("$eles[1],");
        }
    }
    close(PARAS_FILE);
    printf OUT ("%d\n", $ID);
}
close(MM_MATS);
close(OUT);
printf ("Output matrix features to $out\n");
