#!/usr/bin/perl

printf ("Run 1st time:\n");
printf ("c5.0 -f SMAT_float -r > output_float.txt\n");
system ("/home/jli/Work/SMAT/SMAT_collab/serial/offline/C50/c5.0 -f SMAT_float -r > output_float.txt\n");

$rules_file = "./SMAT_float.rules";

open (RULES, $rules_file) || die "Cannot open $rules_file : $!\n";
while ($line = <RULES>)
{
    chomp $line;
    @words_0 = split (/=/, $line);
    @words_1 = split (/\"/, $line);

    if ($words_0[0] eq "rules" )
    {
      $rules_num = $words_1[1];
    }
}
close(RULES);

printf ("Run 2nd time:\n");
printf ("c5.0 -f SMAT_float -r -u %d > ooutput_float.txt\n", $rules_num);
system ("/home/jli/Work/SMAT/SMAT_collab/serial/offline/C50/c5.0 -f SMAT_float -r -u $rules_num > output_float.txt\n");
