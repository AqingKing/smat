#!/usr/bin/perl

printf ("Run 1st time:\n");
printf ("c5.0 -f SMAT_double -r > output_double.txt\n");
system ("/home/jli/Work/SMAT/SMAT_collab/serial/offline/C50/c5.0 -f SMAT_double -r > output_double.txt\n");

$rules_file = "./SMAT_double.rules";

open (RULES, $rules_file) || die "Cannot open $rules_file : $!\n";
while ($line = <RULES>)
{
    chomp $line;
    @words_0 = split (/=/, $line);
    @words_1 = split (/\"/, $line);

    if ($words_0[0] eq "rules" )
    {
      $rules_num = $words_1[1];
    }
}
close(RULES);

printf ("Run 2nd time:\n");
printf ("c5.0 -f SMAT_double -r -u %d > ooutput_double.txt\n", $rules_num);
system ("/home/jli/Work/SMAT/SMAT_collab/serial/offline/C50/c5.0 -f SMAT_double -r -u $rules_num > output_double.txt\n");
