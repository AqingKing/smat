#include <stdio.h>
#include <iostream>
#include <assert.h>
#include <math.h>
#include "config.h"
#include "timer.h"

#include "benchmark_spmv_once.h"

template <typename IndexType, typename ValueType>
int run_once_for_estimate ( csr_matrix<IndexType,ValueType>& csr, int dia_kernel_tag, int ell_kernel_tag, int csr_kernel_tag, int coo_kernel_tag)
{
  int tag_return = 0;
  double confidence[4];
  double max_fill = 20;
  double DIA_perf = 0, ELL_perf = 0, CSR_perf = 0, COO_perf = 0;
  double max_perf = 0;

  confidence[0] = csr.confidence[0];
  confidence[1] = csr.confidence[1];
  confidence[2] = csr.confidence[2];
  confidence[3] = csr.confidence[3];

  if ( confidence[0] != 0 ) //DIA is possible to be the best format
  {
    IndexType max_diags = static_cast<IndexType>( (max_fill * csr.num_nonzeros) / csr.num_rows + 1 );
    printf("\tcreating dia_matrix:");
    dia_matrix<IndexType,ValueType> dia = csr_to_dia<IndexType,ValueType>(csr, max_diags);
    printf("\n");
    if (dia.num_nonzeros == 0 && csr.num_nonzeros != 0)
    {     
	    printf("\tNumber of diagonals (%d) excedes limit (%d)\n", dia.complete_ndiags, max_diags);
	    return -1;
    }

    //Run DIA-SpMV
    if ( dia_kernel_tag == 1 )
      benchmark_spmv_on_host_once ( dia, spmv_dia_serial_host_simple<IndexType, ValueType>,   "dia_serial_simple" );
    DIA_perf = dia.gflops;
    if ( max_perf < DIA_perf )
      max_perf = DIA_perf;
    
    delete_host_matrix(dia);
  }

  if ( confidence[1] != 0 ) //ELL is possible to be the best format
  {
    IndexType max_cols_per_row = static_cast<IndexType>( (max_fill * csr.num_nonzeros) / csr.num_rows + 1 );
    printf("\tcreating ell_matrix:");
    ell_matrix<IndexType,ValueType> ell = csr_to_ell<IndexType,ValueType>(csr, max_cols_per_row);
    printf("\n");
    if (ell.num_nonzeros == 0 && csr.num_nonzeros != 0)
    {      
      printf("\tmax_RD (%d) excedes limit (%d)\n", ell.max_RD, max_cols_per_row);
      return -1;
    }

    //Run ELL-SpMV
    if ( ell_kernel_tag == 1 )
      benchmark_spmv_on_host_once(ell, spmv_ell_serial_host_simple<IndexType, ValueType>,     "ell_serial_simple" );
    ELL_perf = ell.gflops;
    if ( max_perf < ELL_perf )
      max_perf = ELL_perf;

    delete_host_matrix(ell);
  }

  if ( confidence[2] != 0 ) //CSR is possible to be the best format
  {
    //Run CSR-SpMV
    if ( csr_kernel_tag == 1)
      benchmark_spmv_on_host_once(csr, spmv_csr_serial_host_simple<IndexType, ValueType>,     "csr_serial_simple" );
    CSR_perf = csr.gflops;
    if ( max_perf < CSR_perf )
      max_perf = CSR_perf;
  }

  if ( confidence[3] != 0 ) //COO is possible to be the best format
  {
    printf("\tcreating coo_matrix:");
    coo_matrix<IndexType,ValueType> coo = csr_to_coo<IndexType,ValueType>(csr);  
    printf("\n");

    //Run COO-SpMV
    if ( coo_kernel_tag == 1)
      benchmark_spmv_on_host_once(coo, spmv_coo_serial_host_simple<IndexType, ValueType>,     "coo_serial_simple");
    COO_perf = coo.gflops;
    if ( max_perf < COO_perf )
      max_perf = COO_perf;

    delete_host_matrix(coo);
  }

  if ( max_perf == DIA_perf )
    tag_return = 1;
  else if ( max_perf == ELL_perf )
    tag_return = 2;
  else if ( max_perf == CSR_perf )
    tag_return = 3;
  else if ( max_perf == COO_perf )
    tag_return = 4;

  return tag_return;
}
