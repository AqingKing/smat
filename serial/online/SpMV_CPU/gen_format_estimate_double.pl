#!/usr/bin/perl

$DIA_file = "./rules_double/DIA_rules";
$ELL_file = "./rules_double/ELL_rules";
$CSR_file = "./rules_double/CSR_rules";
$COO_file = "./rules_double/COO_rules";
$out_file = ">./format_estimate.h";

print "Output to $out_file\n";
open (OUT, $out_file) || die "Cannot open $out_file : $!\n";
printf OUT ("#include <stdio.h>\n");
printf OUT ("#include <iostream>\n");
printf OUT ("#include <assert.h>\n");
printf OUT ("#include <math.h>\n");
printf OUT ("#include \"config.h\"\n");
printf OUT ("#include \"timer.h\"\n");

printf OUT ("\n");
printf OUT ("#define TDIAGS_TH 0.60\n");
printf OUT ("#define CONFIDENCE_TH 0.90\n");

printf OUT ("\n");
printf OUT ("template <typename IndexType, typename ValueType>\n");
printf OUT ("void format_estimate \( csr_matrix<IndexType, ValueType>& csr\)\n");
printf OUT ("{\n");
printf OUT ("#ifdef TIMING\n");
printf OUT ("\tlong long int Tstart, Tend;\n");
printf OUT ("#endif\n");
printf OUT ("\tIndexType M = csr.num_rows;\n");
printf OUT ("\tIndexType N = csr.num_cols;\n");
printf OUT ("\tIndexType NNZ = csr.num_nonzeros;\n");
printf OUT ("\tdouble aver_RD = \(double\)NNZ / M;\n");
printf OUT ("\tIndexType Ndiags = 0;\n");
printf OUT ("\tIndexType NTdiags = 0;\n");
printf OUT ("\tdouble Tdiags_ratio_TH = TDIAGS_TH;\n");
printf OUT ("\tdouble NTdiags_ratio = 0;\n");
printf OUT ("\tdouble ER_DIA = 0;\n");
printf OUT ("\tIndexType max_RD = 0, min_RD = 0;\n");
printf OUT ("\tdouble dev_RD = 0;\n");
printf OUT ("\tdouble ER_ELL = 0;\n");
printf OUT ("\tdouble R = 0, C = 0;\n");
printf OUT ("\tdouble confidence_TH = CONFIDENCE_TH;\n");
printf OUT ("\tchar *format;\n");

printf OUT ("\n");
printf OUT ("\tIndexType total_count = 0;\n");
printf OUT ("\tIndexType count = 0;\n");
printf OUT ("\tIndexType row_degree;\n");
printf OUT ("\tdouble confidence = 0;\n");
printf OUT ("\tdouble max_confidence = 0;\n");
printf OUT ("\tcsr.confidence[0] = 0;\n");
printf OUT ("\tcsr.confidence[1] = 0;\n");
printf OUT ("\tcsr.confidence[2] = 0;\n");
printf OUT ("\tcsr.confidence[3] = 0;\n");
printf OUT ("\tcsr.tag = 0;\n");

printf OUT ("\n");
printf OUT ("#ifdef TIMING\n");
printf OUT ("\ttimer total_time_struct;\n");
printf OUT ("#endif\n");
printf OUT ("#ifdef PRINT_FEATURES\n");
printf OUT ("\tprintf\(\"M : %%d\\n\", M\);\n");
printf OUT ("\tprintf\(\"N : %%d\\n\", N\);\n");
printf OUT ("\tprintf\(\"NNZ : %%d\\n\", NNZ\);\n");
printf OUT ("#endif\n");


printf OUT ("\n");
printf OUT ("\t//Extract matrix features for DIA\n");
printf OUT ("#ifdef TIMING\n");
printf OUT ("\ttimer DIA_time_struct;\n");
printf OUT ("#endif\n");

printf OUT ("\n");
printf OUT ("\tIndexType *diag_map = new_array<IndexType>\(M + N\);\n");
printf OUT ("\tstd::fill(diag_map, diag_map + M + N, 0);\n");
printf OUT ("\tIndexType *RDs = new_array<IndexType>(M);\n");
printf OUT ("\tmin_RD = csr.Ap[1] - csr.Ap[0];\n");
printf OUT ("\tmax_RD = 0;\n");

printf OUT ("\n");
printf OUT ("\tfor\(IndexType i=0; i<M; i++ \)\n");
printf OUT ("\t{\n");
printf OUT ("\t\tfor\(IndexType jj = csr.Ap[i]; jj < csr.Ap[i+1]; jj++\)\n");
printf OUT ("\t\t{\n");
printf OUT ("\t\t\tIndexType j = csr.Aj[jj];\n");
printf OUT ("\t\t\tIndexType map_index = ( M - i) + j;\n");
printf OUT ("\t\t\tif\(diag_map[map_index] == 0\)\n");
printf OUT ("\t\t\t\tNdiags ++;\n");
printf OUT ("\t\t\tdiag_map[map_index] ++;\n");
printf OUT ("\t\t}\n");
printf OUT ("\t\trow_degree = csr.Ap[i+1] - csr.Ap[i];\n");
printf OUT ("\t\tRDs[i] = row_degree;\n");
printf OUT ("\t\tif \(max_RD < row_degree )\n");
printf OUT ("\t\t\tmax_RD = row_degree;\n");
printf OUT ("\t\tif \(min_RD > row_degree\)\n");
printf OUT ("\t\t\tmin_RD = row_degree;\n");
printf OUT ("\t\tdev_RD += \( row_degree - aver_RD \) * \( row_degree - aver_RD \);\n");
printf OUT ("\t}\n");
printf OUT ("\tdev_RD /= M;\n");
printf OUT ("\tER_DIA = \(double\)NNZ / \(Ndiags * M \);\n");
printf OUT ("\tER_ELL = \(double\)NNZ / \(max_RD * M \);\n");
printf OUT ("#ifdef PRINT_FEATURES\n");
printf OUT ("\tprintf\(\"Ndiags : %%d\\n\", Ndiags\);\n");
printf OUT ("#endif\n");

printf OUT ("\n");
printf OUT ("\tIndexType j = 0;\n");
printf OUT ("\tdouble ratio = 0;\n");
printf OUT ("\tfor \(IndexType i=0; i<M+N; i++ \)\n");
printf OUT ("\t{\n");
printf OUT ("\t\tif \(diag_map[i] != 0 \)\n");
printf OUT ("\t\t{\n");
printf OUT ("\t\t\tj ++;\n");
printf OUT ("\t\t\tratio = \(double\) diag_map[i] / M;\n");
printf OUT ("\t\t\tif \( ratio >= Tdiags_ratio_TH \)\n");
printf OUT ("\t\t\t\tNTdiags ++;\n");
printf OUT ("\t\t}\n");
printf OUT ("\t}\n");
printf OUT ("\tassert ( j == Ndiags );\n");
printf OUT ("\tNTdiags_ratio = \(double\)NTdiags / Ndiags;\n");
printf OUT ("\tfree (diag_map);\n");
printf OUT ("#ifdef PRINT_FEATURES\n");
printf OUT ("\tprintf\(\"NTdiags : %%d\\n\", NTdiags\);\n");
printf OUT ("\tprintf\(\"NTdiags_ratio : %%f\\n\", NTdiags_ratio\);\n");
printf OUT ("\tprintf\(\"ER_DIA : %%f\\n\", ER_DIA\);\n");
printf OUT ("\tprintf\(\"aver_RD : %%f\\n\", aver_RD\);\n");
printf OUT ("\tprintf\(\"max_RD : %%d\\n\", max_RD\);\n");
printf OUT ("\tprintf\(\"min_RD : %%d\\n\", min_RD\);\n");
printf OUT ("\tprintf\(\"dev_RD : %%f\\n\", dev_RD\);\n");
printf OUT ("\tprintf\(\"ER_ELL : %%f\\n\", ER_ELL\);\n");
printf OUT ("#endif\n");

printf OUT ("\n");
printf OUT ("#ifdef TIMING\n");
printf OUT ("\tdouble DIA_time = DIA_time_struct.milliseconds_elapsed\(\);\n");
printf OUT ("#endif\n");

printf OUT ("\n");
printf OUT ("\t//Check the rules of DIA\n");
open (IN_DIA, $DIA_file) || die "Cannot open $DIA_file : $!\n";
while ($line = <IN_DIA>)
{
  chomp $line;
  @words = split (/ /, $line);
  if ( $words[0] ne "Rules" )
  {
    printf OUT ("%s\n", $line);
  }
}
close(IN_DIA);
printf OUT ("\tcsr.confidence[0] = max_confidence;\n");
printf OUT ("\n");

printf OUT ("\t//Check the confidence of DIA\n");
printf OUT ("\tif \( csr.confidence[0] >= confidence_TH \)\n");
printf OUT ("\t{\n");
printf OUT ("\t\tcsr.tag = 1;\n");
printf OUT ("\t\treturn;\n");
printf OUT ("\t}\n");
printf OUT ("\n");

printf OUT ("\n");
printf OUT ("\t//Check the rules of ELL\n");
open (IN_ELL, $ELL_file) || die "Cannot open $ELL_file : $!\n";
while ($line = <IN_ELL>)
{
  chomp $line;
  @words = split (/ /, $line);
  if ( $words[0] ne "Rules" )
  {
    printf OUT ("%s\n", $line);
  }
}
close(IN_ELL);
printf OUT ("\tcsr.confidence[1] = max_confidence;\n");
printf OUT ("\n");

printf OUT ("\t//Check the confidence of ELL\n");
printf OUT ("\tif \( csr.confidence[1] >= confidence_TH \)\n");
printf OUT ("\t{\n");
printf OUT ("\t\tcsr.tag = 2;\n");
printf OUT ("\t\treturn;\n");
printf OUT ("\t}\n");
printf OUT ("\n");

printf OUT ("\n");
printf OUT ("\t//Check the rules of CSR\n");
open (IN_CSR, $CSR_file) || die "Cannot open $CSR_file : $!\n";
while ($line = <IN_CSR>)
{
  chomp $line;
  @words = split (/ /, $line);
  if ( $words[0] ne "Rules" )
  {
    printf OUT ("%s\n", $line);
  }
}
close(IN_CSR);
printf OUT ("\tcsr.confidence[2] = max_confidence;\n");

printf OUT ("\t//Check the confidence of CSR\n");
printf OUT ("\tif \( csr.confidence[2] >= confidence_TH \)\n");
printf OUT ("\t{\n");
printf OUT ("\t\tcsr.tag = 3;\n");
printf OUT ("\t\treturn;\n");
printf OUT ("\t}\n");
printf OUT ("\n");

printf OUT ("\n");
printf OUT ("#ifdef TIMING\n");
printf OUT ("\tprintf \(\"DIA search time : %%8.4lf ms\\n\", DIA_time\);\n");
printf OUT ("#endif\n");

printf OUT ("\n");
printf OUT ("#ifdef TIMING\n");
printf OUT ("\ttimer COO_time_struct;\n");
printf OUT ("#endif\n");
printf OUT ("\tdouble *nzs_distribution = new_array<double>\(N+1);\n");
printf OUT ("\tstd::fill(nzs_distribution, nzs_distribution + N + 1, 0.0\);\n");
printf OUT ("\tIndexType number;\n");
printf OUT ("\tfor \(IndexType i = 0; i < M; i++ \)\n");
printf OUT ("\t{\n");
printf OUT ("\t\tnumber = RDs[i];\n");
printf OUT ("\t\tnzs_distribution[number] += 1;\n");
printf OUT ("\t}\n");
printf OUT ("\ttotal_count = 0;\n");
printf OUT ("\tIndexType peak_pos = 0, peak_RD = 0;\n");
printf OUT ("\tdouble total_sum = 0, peak_ratio = 0;\n");
printf OUT ("\tfor \( IndexType i = 1; i <= N; i++ \)\n");
printf OUT ("\t{\n");
printf OUT ("\t\tif \( nzs_distribution[i] != 0.0 \)\n");
printf OUT ("\t\t{\n");
printf OUT ("\t\t\tnzs_distribution[i] = nzs_distribution[i] / (M-nzs_distribution[0] \);\n");
printf OUT ("\t\t\ttotal_count ++;\n");
printf OUT ("\t\t\ttotal_sum += nzs_distribution[i];\n");
printf OUT ("\t\t\tif \( peak_ratio < nzs_distribution[i] \)\n");
printf OUT ("\t\t\t{\n");
printf OUT ("\t\t\t\tpeak_ratio = nzs_distribution[i];\n");
printf OUT ("\t\t\t\tpeak_pos = total_count;\n");
printf OUT ("\t\t\t\tpeak_RD = i;\n");
printf OUT ("\t\t\t}\n");
printf OUT ("\t\t}\n");
printf OUT ("\t}\n");

printf OUT ("\n");
printf OUT ("\tcount = 0;\n");
printf OUT ("\tdouble aver_x = 0, aver_y = 0;\n");
printf OUT ("\tfor\(IndexType i = peak_RD; i <= N; i++ \)\n");
printf OUT ("\t{\n");
printf OUT ("\t\tif \( nzs_distribution[i] != 0.0 \)\n");
printf OUT ("\t\t{\n");
printf OUT ("\t\t\tcount ++;\n");
printf OUT ("\t\t\taver_x += log10\(i\);\n");
printf OUT ("\t\t\taver_y += log10\(nzs_distribution[i]\);\n");
printf OUT ("\t\t}\n");
printf OUT ("\t}\n");
printf OUT ("\tassert \( count == \(total_count - peak_pos + 1\) \);\n");
printf OUT ("\taver_x /= count;\n");
printf OUT ("\taver_y /= count;\n");

printf OUT ("\n");
printf OUT ("\tdouble a_up = 0, a_down = 0;\n");
printf OUT ("\tdouble a = 0, b = 0;\n");
printf OUT ("\tfor \( IndexType i = peak_RD; i<= N; i++ \)\n");
printf OUT ("\t{\n");
printf OUT ("\t\tif \( nzs_distribution[i] != 0.0 \)\n");
printf OUT ("\t\t{\n");
printf OUT ("\t\t\ta_up += \(log10\(i\) - aver_x\) * \(log10\(nzs_distribution[i]\) - aver_y\);\n");
printf OUT ("\t\t\ta_down += \(log10\(i\) -aver_x\) * \(log10\(i\) - aver_x\);\n");
printf OUT ("\t\t}\n");
printf OUT ("\t}\n");
printf OUT ("\ta = a_up / a_down;\n");
printf OUT ("\tb = aver_y - a * aver_x;\n");
printf OUT ("\tR = 0 - a;\n");
printf OUT ("\tC = pow\( 10, b\);\n");
printf OUT ("#ifdef PRINT_FEATURES\n");
printf OUT ("\tprintf\(\"R : %%f\\n\", R\);\n");
printf OUT ("#endif\n");
printf OUT ("#ifdef TIMING\n");
printf OUT ("\tdouble COO_time = COO_time_struct.milliseconds_elapsed\(\);\n");
printf OUT ("#endif\n");
printf OUT ("\tdouble test_sum = 0;\n");
printf OUT ("\tfor \( IndexType i = 1; i < peak_RD; i ++ )\n");
printf OUT ("\t\tif \( nzs_distribution[i] != 0.0 \)\n");
printf OUT ("\t\t{\n");
printf OUT ("\t\t\ttest_sum += nzs_distribution[i];\n");
printf OUT ("\t\t}\n");
printf OUT ("\tfor \( IndexType i = peak_RD; i <= N; i++ \)\n");
printf OUT ("\t\tif \( nzs_distribution[i] != 0.0 \)\n");
printf OUT ("\t\t{\n");
printf OUT ("\t\t\ttest_sum += \( C * pow\(i, 0-R\) \);\n");
printf OUT ("\t\t}\n");
printf OUT ("\tfree \( nzs_distribution \);\n");
printf OUT ("\tfree \( RDs \);\n");

printf OUT ("\n");
printf OUT ("\t//Check the rules of COO\n");
open (IN_COO, $COO_file) || die "Cannot open $COO_file : $!\n";
while ($line = <IN_COO>)
{
  chomp $line;
  @words = split (/ /, $line);
  if ( $words[0] ne "Rules" )
  {
    printf OUT ("%s\n", $line);
  }
}
close(IN_COO);
printf OUT ("\tcsr.confidence[3] = max_confidence;\n");
printf OUT ("\n");

printf OUT ("\t//Check the confidence of COO\n");
printf OUT ("\tif \( csr.confidence[3] >= confidence_TH \)\n");
printf OUT ("\t{\n");
printf OUT ("\t\tcsr.tag = 4;\n");
printf OUT ("\t\treturn;\n");
printf OUT ("\t}\n");
printf OUT ("\n");

printf OUT ("\n");
printf OUT ("#ifdef TIMING\n");
printf OUT ("\tprintf \(\"COO search time : %%8.4lf ms\\n\", COO_time\);\n");
printf OUT ("#endif\n");


printf OUT ("\n");
printf OUT ("\t//If need to run SpMV one time, run it in driver.cpp file\n");

printf OUT ("\n");
printf OUT ("#ifdef TIMING\n");
printf OUT ("\tdouble total_time = total_time_struct.milliseconds_elapsed\(\);\n");
printf OUT ("\tprintf\(\"\\nTotal search time : %%8.4lf ms\\n\", total_time\);\n");
printf OUT ("\tprintf\(\"\\n CSR time for comparision: \\n\"\);\n");
printf OUT ("\tcsr_perf = test_once_csr_matrix_kernels\(csr\);\n");
printf OUT ("#endif\n");

printf OUT ("\n");
printf OUT ("\treturn;\n");
printf OUT ("}\n");


close (OUT);
