#include <stdio.h>
#include <iostream>
#include <assert.h>
#include <math.h>
#include "config.h"
#include "timer.h"

#define TDIAGS_TH 0.60
#define CONFIDENCE_TH 0.90

template <typename IndexType, typename ValueType>
void format_estimate ( csr_matrix<IndexType, ValueType>& csr)
{
#ifdef TIMING
	long long int Tstart, Tend;
#endif
	IndexType M = csr.num_rows;
	IndexType N = csr.num_cols;
	IndexType NNZ = csr.num_nonzeros;
	double aver_RD = (double)NNZ / M;
	IndexType Ndiags = 0;
	IndexType NTdiags = 0;
	double Tdiags_ratio_TH = TDIAGS_TH;
	double NTdiags_ratio = 0;
	double ER_DIA = 0;
	IndexType max_RD = 0, min_RD = 0;
	double dev_RD = 0;
	double ER_ELL = 0;
	double R = 0, C = 0;
	double confidence_TH = CONFIDENCE_TH;
	char *format;

	IndexType total_count = 0;
	IndexType count = 0;
	IndexType row_degree;
	double confidence = 0;
	double max_confidence = 0;
	csr.confidence[0] = 0;
	csr.confidence[1] = 0;
	csr.confidence[2] = 0;
	csr.confidence[3] = 0;
	csr.tag = 0;

#ifdef TIMING
	timer total_time_struct;
#endif
#ifdef PRINT_FEATURES
	printf("M : %d\n", M);
	printf("N : %d\n", N);
	printf("NNZ : %d\n", NNZ);
#endif

	//Extract matrix features for DIA
#ifdef TIMING
	timer DIA_time_struct;
#endif

	IndexType *diag_map = new_array<IndexType>(M + N);
	std::fill(diag_map, diag_map + M + N, 0);
	IndexType *RDs = new_array<IndexType>(M);
	min_RD = csr.Ap[1] - csr.Ap[0];
	max_RD = 0;

	for(IndexType i=0; i<M; i++ )
	{
		for(IndexType jj = csr.Ap[i]; jj < csr.Ap[i+1]; jj++)
		{
			IndexType j = csr.Aj[jj];
			IndexType map_index = ( M - i) + j;
			if(diag_map[map_index] == 0)
				Ndiags ++;
			diag_map[map_index] ++;
		}
		row_degree = csr.Ap[i+1] - csr.Ap[i];
		RDs[i] = row_degree;
		if (max_RD < row_degree )
			max_RD = row_degree;
		if (min_RD > row_degree)
			min_RD = row_degree;
		dev_RD += ( row_degree - aver_RD ) * ( row_degree - aver_RD );
	}
	dev_RD /= M;
	ER_DIA = (double)NNZ / (Ndiags * M );
	ER_ELL = (double)NNZ / (max_RD * M );
#ifdef PRINT_FEATURES
	printf("Ndiags : %d\n", Ndiags);
#endif

	IndexType j = 0;
	double ratio = 0;
	for (IndexType i=0; i<M+N; i++ )
	{
		if (diag_map[i] != 0 )
		{
			j ++;
			ratio = (double) diag_map[i] / M;
			if ( ratio >= Tdiags_ratio_TH )
				NTdiags ++;
		}
	}
	assert ( j == Ndiags );
	NTdiags_ratio = (double)NTdiags / Ndiags;
	free (diag_map);
#ifdef PRINT_FEATURES
	printf("NTdiags : %d\n", NTdiags);
	printf("NTdiags_ratio : %f\n", NTdiags_ratio);
	printf("ER_DIA : %f\n", ER_DIA);
	printf("aver_RD : %f\n", aver_RD);
	printf("max_RD : %d\n", max_RD);
	printf("min_RD : %d\n", min_RD);
	printf("dev_RD : %f\n", dev_RD);
	printf("ER_ELL : %f\n", ER_ELL);
#endif

#ifdef TIMING
	double DIA_time = DIA_time_struct.milliseconds_elapsed();
#endif

	//Check the rules of DIA
if ( ( NNZ <= 18488476.000000 ) && ( ER_DIA > 0.435216 ) )
{
	format = "DIA";
	confidence = 0.987000;
	if ( max_confidence < confidence )
		max_confidence = confidence;
}
if ( ( Ndiags <= 39.000000 ) && ( ER_DIA > 0.259082 ) && ( ER_ELL <= 0.900924 ) )
{
	format = "DIA";
	confidence = 0.980000;
	if ( max_confidence < confidence )
		max_confidence = confidence;
}
	csr.confidence[0] = max_confidence;

	//Check the confidence of DIA
	if ( csr.confidence[0] >= confidence_TH )
	{
		csr.tag = 1;
		return;
	}


	//Check the rules of ELL
if ( ( NNZ > 13828.000000 ) && ( ER_DIA <= 0.259082 ) && ( max_RD <= 7.000000 ) && ( ER_ELL > 0.776856 ) )
{
	format = "ELL";
	confidence = 0.938000;
	if ( max_confidence < confidence )
		max_confidence = confidence;
}
if ( ( M <= 503.000000 ) && ( ER_DIA <= 0.259082 ) && ( max_RD <= 6.000000 ) && ( ER_ELL > 0.776856 ) )
{
	format = "ELL";
	confidence = 0.821000;
	if ( max_confidence < confidence )
		max_confidence = confidence;
}
	csr.confidence[1] = max_confidence;

	//Check the confidence of ELL
	if ( csr.confidence[1] >= confidence_TH )
	{
		csr.tag = 2;
		return;
	}


	//Check the rules of CSR
if ( ( Ndiags > 39.000000 ) )
{
	format = "CSR";
	confidence = 0.681000;
	if ( max_confidence < confidence )
		max_confidence = confidence;
}
	csr.confidence[2] = max_confidence;
	//Check the confidence of CSR
	if ( csr.confidence[2] >= confidence_TH )
	{
		csr.tag = 3;
		return;
	}


#ifdef TIMING
	printf ("DIA search time : %8.4lf ms\n", DIA_time);
#endif

#ifdef TIMING
	timer COO_time_struct;
#endif
	double *nzs_distribution = new_array<double>(N+1);
	std::fill(nzs_distribution, nzs_distribution + N + 1, 0.0);
	IndexType number;
	for (IndexType i = 0; i < M; i++ )
	{
		number = RDs[i];
		nzs_distribution[number] += 1;
	}
	total_count = 0;
	IndexType peak_pos = 0, peak_RD = 0;
	double total_sum = 0, peak_ratio = 0;
	for ( IndexType i = 1; i <= N; i++ )
	{
		if ( nzs_distribution[i] != 0.0 )
		{
			nzs_distribution[i] = nzs_distribution[i] / (M-nzs_distribution[0] );
			total_count ++;
			total_sum += nzs_distribution[i];
			if ( peak_ratio < nzs_distribution[i] )
			{
				peak_ratio = nzs_distribution[i];
				peak_pos = total_count;
				peak_RD = i;
			}
		}
	}

	count = 0;
	double aver_x = 0, aver_y = 0;
	for(IndexType i = peak_RD; i <= N; i++ )
	{
		if ( nzs_distribution[i] != 0.0 )
		{
			count ++;
			aver_x += log10(i);
			aver_y += log10(nzs_distribution[i]);
		}
	}
	assert ( count == (total_count - peak_pos + 1) );
	aver_x /= count;
	aver_y /= count;

	double a_up = 0, a_down = 0;
	double a = 0, b = 0;
	for ( IndexType i = peak_RD; i<= N; i++ )
	{
		if ( nzs_distribution[i] != 0.0 )
		{
			a_up += (log10(i) - aver_x) * (log10(nzs_distribution[i]) - aver_y);
			a_down += (log10(i) -aver_x) * (log10(i) - aver_x);
		}
	}
	a = a_up / a_down;
	b = aver_y - a * aver_x;
	R = 0 - a;
	C = pow( 10, b);
#ifdef PRINT_FEATURES
	printf("R : %f\n", R);
#endif
#ifdef TIMING
	double COO_time = COO_time_struct.milliseconds_elapsed();
#endif
	double test_sum = 0;
	for ( IndexType i = 1; i < peak_RD; i ++ )
		if ( nzs_distribution[i] != 0.0 )
		{
			test_sum += nzs_distribution[i];
		}
	for ( IndexType i = peak_RD; i <= N; i++ )
		if ( nzs_distribution[i] != 0.0 )
		{
			test_sum += ( C * pow(i, 0-R) );
		}
	free ( nzs_distribution );
	free ( RDs );

	//Check the rules of COO
if ( ( NNZ <= 11407.000000 ) && ( dev_RD <= 5.917887 ) && ( ER_ELL <= 0.438617 ) )
{
	format = "COO";
	confidence = 0.991000;
	if ( max_confidence < confidence )
		max_confidence = confidence;
}
if ( ( NNZ <= 11407.000000 ) && ( max_RD > 1027.000000 ) )
{
	format = "COO";
	confidence = 0.984000;
	if ( max_confidence < confidence )
		max_confidence = confidence;
}
if ( ( ER_DIA <= 0.022470 ) && ( min_RD <= 3.000000 ) && ( dev_RD <= 9.000000 ) && ( ER_ELL <= 0.314657 ) && ( R > 2.847474 ) )
{
	format = "COO";
	confidence = 0.957000;
	if ( max_confidence < confidence )
		max_confidence = confidence;
}
if ( ( NNZ > 11407.000000 ) && ( max_RD <= 19.000000 ) && ( min_RD <= 3.000000 ) && ( ER_ELL <= 0.499750 ) && ( R > 2.847474 ) )
{
	format = "COO";
	confidence = 0.975000;
	if ( max_confidence < confidence )
		max_confidence = confidence;
}
if ( ( ER_DIA <= 0.259082 ) && ( max_RD <= 7.000000 ) && ( min_RD <= 1.000000 ) && ( ER_ELL <= 0.776856 ) )
{
	format = "COO";
	confidence = 0.900000;
	if ( max_confidence < confidence )
		max_confidence = confidence;
}
if ( ( NNZ <= 11407.000000 ) && ( NTdiags_ratio <= 0.000289 ) && ( ER_DIA <= 0.005895 ) && ( dev_RD <= 588.922060 ) && ( ER_ELL <= 0.060768 ) )
{
	format = "COO";
	confidence = 0.962000;
	if ( max_confidence < confidence )
		max_confidence = confidence;
}
if ( ( M > 503.000000 ) && ( NNZ <= 13828.000000 ) && ( Ndiags <= 1753.000000 ) && ( ER_DIA <= 0.259082 ) && ( max_RD <= 6.000000 ) && ( min_RD <= 3.000000 ) )
{
	format = "COO";
	confidence = 0.889000;
	if ( max_confidence < confidence )
		max_confidence = confidence;
}
if ( ( Ndiags <= 806257.000000 ) && ( NTdiags_ratio <= 0.000013 ) && ( dev_RD > 9.000000 ) && ( dev_RD <= 34.949875 ) && ( ER_ELL <= 0.095755 ) )
{
	format = "COO";
	confidence = 0.957000;
	if ( max_confidence < confidence )
		max_confidence = confidence;
}
if ( ( NNZ <= 11407.000000 ) && ( ER_DIA <= 0.259082 ) && ( max_RD <= 9.000000 ) && ( ER_ELL <= 0.776856 ) )
{
	format = "COO";
	confidence = 0.938000;
	if ( max_confidence < confidence )
		max_confidence = confidence;
}
if ( ( NNZ <= 307858.000000 ) && ( ER_DIA <= 0.000769 ) && ( min_RD <= 1.000000 ) && ( dev_RD > 39.473541 ) && ( dev_RD <= 429.502780 ) && ( ER_ELL <= 0.213519 ) )
{
	format = "COO";
	confidence = 0.875000;
	if ( max_confidence < confidence )
		max_confidence = confidence;
}
if ( ( NNZ <= 11407.000000 ) && ( dev_RD <= 15.588347 ) && ( ER_ELL <= 0.267337 ) )
{
	format = "COO";
	confidence = 0.935000;
	if ( max_confidence < confidence )
		max_confidence = confidence;
}
	csr.confidence[3] = max_confidence;

	//Check the confidence of COO
	if ( csr.confidence[3] >= confidence_TH )
	{
		csr.tag = 4;
		return;
	}


#ifdef TIMING
	printf ("COO search time : %8.4lf ms\n", COO_time);
#endif

	//If need to run SpMV one time, run it in driver.cpp file

#ifdef TIMING
	double total_time = total_time_struct.milliseconds_elapsed();
	printf("\nTotal search time : %8.4lf ms\n", total_time);
	printf("\n CSR time for comparision: \n");
	csr_perf = test_once_csr_matrix_kernels(csr);
#endif

	return;
}
