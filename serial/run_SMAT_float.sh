#!/bin/bash

# Notice the path in the perl files.

printf "## offline process ##\n";

printf "## data mining ##\n";
printf "### Extract matrix features ###\n";
cd ./offline/SpMV_CPU; pwd;
./run_float.pl
printf "### Extract data file from learning matrix set ###\n";
cd ../data_mining; pwd;
./gen_data_learn_fllat.pl
printf "### Extract test file from testing matrix set ###\n";
./gen_data_test_float.pl
printf "### Running C5.0 ###\n";
./run_C50_float.pl

printf "\n## online process ##\n";
cd ../../online/SpMV_CPU;
pwd
printf "### Generate rules separately ###\n";
./gen_rules_float.pl
printf "### Generate format_estimate.h ###\n";
./gen_format_estimate_float.pl
printf "### Online run SpMV ###\n";
./run_float.pl

printf "## Record Performance ##\n";
# Compare with CSR
# Compare to MKL

# Accuracy test
# With the best format
# decision tree accuray

# Timing 
# search time
