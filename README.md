# README #

SMAT source code

### Code structure ###

* SpMV_init
    * SpMV code in four formats, CSR, COO, ELL, and DIA.
* serial -- sequential SMAT
    * Run scripts: run_SMAT_float.sh (or run_SMAT_double.sh), but suggest run the content of scripts step-by-step personally.
    * offline:
        * C50: software to generate decision tree.
        * data_mining: Generate learning and testing data, and then using C50 to build a decision tree.
        * SpMV_CPU: sequential SpMV code, the same with SpMV_init.
        * Mats_features_float (Mats_features_double): Record the results of four formats SpMV on training matrices.
        * formats_comparision: a simple script to compare performance among four formats.
    * online:
        * SpMV_CPU: test sequential SpMV performance on testing matrices.
            * gen_rules_float.pl (gen_rules_double.pl): generate separate rules for the four formats, which is the model of SMAT.
            * gen_format_estimate_float.pl (gen_format_estimate_double.pl): generate format_estimate.h file using the rules.
            * run_float.pl (run_double.pl): Run four formats SpMV on testing matrices, using the model.
* omp - OMP SMAT
    * online: OMP SMAT only has online code, we're using the same model with the serial SMAT.
        * SpMV_CPU: the same content with serial->online->SpMV_CPU
        * extract_perf_data: a simple script 
* mats_list - matrix set and some scripts.
* perf_comparison
    * test_MKL: test MKL performance for comparison.

### Contributors ###

* Jiajia Li (jiajiali@gatech.edu)